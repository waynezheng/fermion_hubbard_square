 #!/bin/bash

X=06
Y=01
UE=02
DE=02
U=2.0
SPEC=10
NUMSAM=100

 mv eigenvalues.dat eigenvalues${SPEC}_bowtie_u${U}_${U}_${NUMSAM}_filling${UE}${DE}.dat 
 mv eigenvectors.dat eigenvectors${SPEC}_bowtie_u${U}_${U}_${NUMSAM}_filling${UE}${DE}.dat 
 mv hilbert_space.dat hilbert_space_bowtie_filling${UE}${DE}.txt
 # mv hilbert_space.dat hilbert_space_lattice${X}${Y}PO_u${U}_filling${UE}${DE}.txt

 mv *.txt /Users/wayne/Downloads/data/hubbard
 mv *.dat /Users/wayne/Downloads/data/hubbard

