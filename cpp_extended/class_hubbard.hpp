// A class is an object. Here the object Hubbard denotes a quantum system consisting of a specific lattce and kind of Hamiltoian.i
template<typename T>
class Hubbard {
        private:
        // Paramaters.
        double mU; // Hubbard U. t=1.0 is set as energy unit. 
        double mV;
        int mNumUp; // Number of spin-up electrons. 
        int mNumDown; // Number of spin-down electrons.
        // Define the lattice.
        int mNumSiteX;
        int mNumSiteY;
        int mNumSite;
        std::string mBouConX;
        std::string mBouConY;
        // Dimension and basis of the Hilbert.
        long long int mDim;
        std::vector<int> mBasis;

        std::vector<int> mSite;
        std::vector<std::vector<int>> mSiteForward;
        std::vector<T> mHop;

        int mNzero; // # of non-zero matrix elements.
        std::vector<T> mA;
        std::vector<int> mIrow; // Index of row.
        std::vector<int> mIcol; // Index of column. 

        public:
        Hubbard (double u, double v, int numUp, int numDown, int numSiteX, int numSiteY, std::string bouConX, std::string bouConY); // Constructor.
        ~Hubbard (); // Destructor.
        
        int Dim ();
        void SetNegativeLink (int i, int z);
        void SetLink (int i, int j, int z, T a);
        void CSCHamMatrix ();
        void Mv (T* v, T* w);
        void Hamiltonian (T* v, T* w);
        void Matrix(int &nnz, T* &A, int* &irow, int* &ipcol);
        void TotalSMinusSPlus(T* v, T* w);
        void TotalSPlusSMinus(T* v, T* w);
        void SZSZ(T* v, T* w, int i, int j);

        void SaveHilbert();
        void SetOne(T* v, int i);
        T Dot(T* v, T* w);
        void PrintHam();
        };

// To define a specific quantum system and build up the corresponding Hilbert space in the tensor product representation.
template<typename T>
Hubbard<T>::Hubbard (double u, double v, int numUp, int numDown, int numSiteX, int numSiteY, std::string bouConX, std::string bouConY) {
        mU = u;
        mV = v;
        mNumUp = numUp;
        mNumDown = numDown;
        mNumSiteX = numSiteX;
        mNumSiteY = numSiteY;
        mNumSite = mNumSiteX*mNumSiteY;
        mBouConX = bouConX;
        mBouConY = bouConY;
        // Construct the basis of the Hilbert space constrained by the U(1) symmetry.
        long long int max = int(pow(2, mNumSite));
        long long int dim = 0;
        for (long long int i = 0; i < max; ++i) {
            boost::dynamic_bitset<> bitUp (mNumSite, i);
            if (int(bitUp.count()) == numUp) {
                for (long long int j = 0; j < max; ++j) {
                    boost::dynamic_bitset<> bitDown (mNumSite, j);
                    if (int(bitDown.count()) == numDown) {
                        boost::dynamic_bitset<> bit;
                        for (int k = 0; k < mNumSite; ++k) { bit.push_back(bitUp[k]); } // Basis of tensor product of up- and down-spins.
                        for (int k = 0; k < mNumSite; ++k) { bit.push_back(bitDown[k]); }
                        mBasis.push_back(bit.to_ulong());
                        ++dim;
                        }
                    }
                }
            }
        std::sort (mBasis.begin(), mBasis.end());
        mDim = dim;
        // Initialize hopping amplitude.
        for (int j = 0; j < mNumSiteY; ++j) {
            for (int i = 0; i < mNumSiteX; ++i) {
                mSite.push_back(j*mNumSiteX+i);
                std::vector<int> temp;
                temp.push_back (j*mNumSiteX+((i+1) % mNumSiteX));
                temp.push_back (((j+1) % mNumSiteY)*mNumSiteX+i);
                mSiteForward.push_back (temp);
                mHop.push_back (1.0);
                mHop.push_back (1.0);
                }
            }
        }

template<typename T>
Hubbard<T>::~Hubbard () {}

template<typename T>
int Hubbard<T>::Dim () { return mDim; }

template<typename T>
void Hubbard<T>::SetLink (int i, int j, int z, T a) {
        int k = j*mNumSiteX+i;
        mHop[k*2+z] = a;
        }

// Construct the sparse matrix.
template<typename T>
void Hubbard<T>::CSCHamMatrix () {
        int count = 0;
        for (int l = 0; l < mDim; ++l) {
            int b = mBasis[l];
            boost::dynamic_bitset<> config (mNumSite*2, b);
            std::map<int, T, std::less<int>> tempCol; // Sorted map in asending order.
            for (int cs = 0; cs < mNumSite; ++cs) { // Current site.
                // Hubbard U.
                if (config[cs] & config[mNumSite+cs]) {
                    auto itU = tempCol.find (l);
                    if (itU == tempCol.end()) { tempCol.insert (std::make_pair (l, mU)); }
                    else { itU->second += mU; }
                    }
                for (int move = 0; move < 2; ++ move) {
                        int fs = mSiteForward[cs][move];
                        if (fs > cs) {
                            int nCs = config[cs]+config[mNumSite+cs];
                            int nFs = config[fs]+config[mNumSite+fs];
                            auto itU = tempCol.find (l);
                            }
                        else if ((mBouConX == "PBC") && ((mNumSiteX-1) == (cs-fs))) {
                            }
                    }
                // Hopping.
                for (int spin = 0; spin < 2; ++spin) {
                    // Move forward.
                    for (int move = 0; move < 2; ++move) {
                        int fs = mSiteForward[cs][move];
                        int current = spin*mNumSite+cs;
                        int forward = spin*mNumSite+fs;
                        if ((config[current] ^ config[forward])) {
                            T phase = 0.0;
                            if (1 == config[current]) { phase = mHop[cs*2+move]; }
                            else { phase = std::conj (mHop[cs*2+move]); }
                            // For OBC on both directions.
                            if (fs > cs) {
                                double fSign = 1.0;
                                for (int m = current+1; m < forward; ++m) { if (config[m]) { fSign = -1.0*fSign; } }
                                boost::dynamic_bitset<> temp (config);
                                temp.flip (current);
                                temp.flip (forward);
                                std::vector<int>::iterator itFlip = std::lower_bound (mBasis.begin(), mBasis.end(), int(temp.to_ulong()));
                                int k = std::distance (mBasis.begin(), itFlip);
                                auto itHop = tempCol.find (k);
                                if (itHop == tempCol.end()) { tempCol.insert (std::make_pair(k, -1.0*phase*fSign)); }
                                else { itHop->second += -1.0*phase*fSign; }
                                }
                            // Cope with boundary hoppings.
                            else if ((mBouConX == "PBC") && ((mNumSiteX-1) == (cs-fs))) {
                                double fSign = 1.0;
                                for (int m = forward+1; m < current; ++m) { if (config[m]) { fSign = -1.0*fSign; } }
                                boost::dynamic_bitset<> temp (config);
                                temp.flip (current);
                                temp.flip (forward);
                                std::vector<int>::iterator itFlip = std::lower_bound (mBasis.begin (), mBasis.end (), int (temp.to_ulong()));
                                int k = std::distance (mBasis.begin (), itFlip);
                                auto itHop = tempCol.find(k);
                                if (itHop == tempCol.end()) { tempCol.insert (std::make_pair(k, -1.0*phase*fSign)); }
                                else { itHop->second += -1.0*phase*fSign; }
                                }
                            else if ((mBouConY == "PBC") && ((mNumSiteY-1)*mNumSiteX == (cs-fs))) {
                                double fSign = 1.0;
                                for (int m = forward+1; m < current; ++m) { if (config[m]) { fSign = -1.0*fSign; } }
                                boost::dynamic_bitset<> temp (config);
                                temp.flip (current);
                                temp.flip (forward);
                                std::vector<int>::iterator itFlip = std::lower_bound (mBasis.begin(), mBasis.end(), int(temp.to_ulong()));
                                int k = std::distance (mBasis.begin(), itFlip);
                                auto itHop = tempCol.find (k);
                                if (itHop == tempCol.end()) { tempCol.insert (std::make_pair (k, -1.0*phase*fSign)); }
                                else { itHop->second += -1.0*phase*fSign; }
                                }
                            }
                        }
                    }
                }
            for (auto it = tempCol.begin(); it != tempCol.end(); ++it) {
                mIcol.push_back (l);
                mIrow.push_back (it->first);
                mA.push_back (it->second);
                ++count;
                }
            }
        mNzero = count;
        std::cout << mNzero << std::endl;
        }

// Required by Arpack++ package handbook: There only requirements make by ARPACK++ are that member funtion Hamiltonian() musth have two pointers to vectors of type T as paraments and the input vector must precede the output vector.
template<typename T>
void Hubbard<T>::Mv (T* v, T* w) {
        std::fill_n (w, mDim, 0.0);
        for (int l = 0; l < mNzero; ++l) { w[mIrow[l]] += mA[l]*v[mIcol[l]]; }
        }

template<typename T>
void Hubbard<T>::TotalSMinusSPlus (T* v, T* w) {
        for (int l = 0; l < mDim; ++l) { w[l] = 0.0; }
        for (int l = 0; l < mDim; ++l) {
            if (0.0 == v[l]) { continue; }
            int b = mBasis[l];
            boost::dynamic_bitset<> config(mNumSite*2, b);
            for (int i = 0; i < mNumSite; ++i) {
                for (int j = 0; j < mNumSite; ++j) {
                    boost::dynamic_bitset<> temp(config);
                    if (i == j) {
                        w[l] += double(config[mNumSite+j]-config[j]*config[mNumSite+j])*v[l];
                        }
                    else {
                        int upI = i;
                        int upJ = j;
                        int downI = mNumSite+i;
                        int downJ = mNumSite+j;
                        if (1 == config[upI] && 0 == config[upJ] && 1 == config[downJ] && 0 == config[downI]) {
                            int upCross = 0;
                            int downCross = 0;
                            if (i < j) {
                                for (int m = upI+1; m < upJ; ++m) { if (config[m]) { ++upCross; } }
                                for (int m = downJ-1; m > downI; --m) { if (config[m]) { ++downCross; } }
                                }
                            else {
                                for (int m = upI-1; m > upJ; --m) { if (config[m]) { ++upCross; } }
                                for (int m = downJ+1; m < downI; ++m) { if (config[m]) { ++downCross; } }
                                }
                            double fSign = pow(-1.0, upCross+downCross);
                            temp.flip(upI);
                            temp.flip(upJ);
                            temp.flip(downI);
                            temp.flip(downJ);
                            std::vector<int>::iterator it = std::lower_bound(mBasis.begin(), mBasis.end(), int(temp.to_ulong()));
                            w[std::distance(mBasis.begin(), it)] += -1.0*fSign*v[l];
                            }
                        }
                    }
                }
            }
        }

template<typename T>
void Hubbard<T>::TotalSPlusSMinus (T* v, T* w) {
        std::fill_n(w, mDim, 0.0);
        for (int l = 0; l < mDim; ++l) {
            if (0.0 == v[l]) { continue; }
            int b = mBasis[l];
            boost::dynamic_bitset<> config(mNumSite*2, b);
            for (int i = 0; i < mNumSite; ++i) {
                for (int j = 0; j < mNumSite; ++j) {
                    boost::dynamic_bitset<> temp(config);
                    if (i == j) {
                        w[l] += double(config[j]-config[j]*config[mNumSite+j])*v[l];
                        }
                    else {
                        int upI = i;
                        int upJ = j;
                        int downI = mNumSite+i;
                        int downJ = mNumSite+j;
                        if (1 == config[upJ] && 0 == config[upI] && 1 == config[downI] && 0 == config[downJ]) {
                            int upCross = 0;
                            int downCross = 0;
                            if (i < j) {
                                for (int m = upJ-1; m > upI; --m) { if (config[m]) { ++upCross; } }
                                for (int m = downI+1; m < downJ; ++m) { if (config[m]) { ++downCross; } }
                                }
                            else {
                                for (int m = upJ+1; m < upI; ++m) { if (config[m]) { ++upCross; } }
                                for (int m = downI-1; m > downJ; --m) { if (config[m]) { ++downCross; } }
                                }
                            double fSign = pow(-1.0, upCross+downCross);
                            temp.flip(upI);
                            temp.flip(upJ);
                            temp.flip(downI);
                            temp.flip(downJ);
                            std::vector<int>::iterator it = std::lower_bound(mBasis.begin(), mBasis.end(), int(temp.to_ulong()));
                            w[std::distance(mBasis.begin(), it)] += -1.0*fSign*v[l];
                            }
                        }
                    }
                }
            }
        }

template<typename T>
void Hubbard<T>::SZSZ(T* v, T* w, int i, int j) {
        std::fill_n(w, mDim, 0.0);
        for (int l = 0; l < mDim; ++l) {
            if (0.0 == v[l]) { continue; }
            int b = mBasis[l];
            boost::dynamic_bitset<> config(mNumSite*2, b);
            w[l] += 0.25*(config[i]-config[mNumSite+i])*(config[j]-config[mNumSite+j])*v[l];
            }
        }

template<typename T>
void Hubbard<T>::SaveHilbert () {
        std::ofstream file_hilbert ("hilbert_space.dat", std::ios_base::app);
        for (int l = 0; l < mDim; ++l) {
            file_hilbert << mBasis[l];
            if (l != (mDim-1)) { file_hilbert << ","; }
            }
        file_hilbert.close();
        }

template<typename T>
void Hubbard<T>::SetOne (T* v, int i) {
        std::fill_n(v, mDim, 0.0);
        v[i] = 1.0;
        }

template<typename T>
T Hubbard<T>::Dot (T* v, T* w) {
        T r = 0.0;
        for (int l = 0; l < mDim; ++l) { r += std::conj(v[l])*w[l]; }
        return r;
        }

template<typename T>
void Hubbard<T>::PrintHam () {
        std::ofstream file_ham ("ham.dat", std::ios_base::app | std::ios_base::binary);
        auto u = new T[mDim];
        auto v = new T[mDim];
        auto w = new T[mDim];
        for (int i = 0; i < mDim; ++i) {
            for (int j = 0; j < mDim; ++j) {
                SetOne (v, i);
                SetOne (w, j);
                Mv (w, u);
                // std::cout << std::real(Dot(v, u)) << ", ";
                T val = Dot (v, u);
                double rp = std::real (val);
                double ip = std::imag (val);
                file_ham.write ((char*)(&rp), sizeof (double));
                file_ham.write ((char*)(&ip), sizeof (double));
                }
            // std::cout << std::endl;
            }
        // std::cout << std::endl;
        file_ham.close ();
        delete [] u;
        delete [] v;
        delete [] w;
        }
