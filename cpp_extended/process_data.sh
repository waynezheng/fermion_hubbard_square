 #!/bin/bash

X=08
Y=01
UE=04
DE=04
U=100.0
SPEC=10

START=0.2
STEP=0.2
NUMSAM=100

 mv eigenvalues.dat eigenvalues${SPEC}_lattice${X}${Y}PO_u${U}_filling${UE}${DE}_phiNum${NUMSAM}.dat 
 mv eigenvectors.dat eigenvectors${SPEC}_lattice${X}${Y}PO_u${U}_filling${UE}${DE}_phiNum${NUMSAM}.dat 
 # mv hilbert_space.dat hilbert_space_lattice${X}${Y}PO_u${U}_filling${UE}${DE}.txt
 # mv hilbert_space.dat hilbert_space_lattice${X}${Y}PO_u${U}_filling${UE}${DE}.txt

 mv *.dat /Users/wayne/Downloads/data/hubbard

