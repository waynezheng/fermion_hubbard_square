#include "head.hpp"
#include "class_hubbard.hpp"

int main() {
        const int LINE = 0;

        std::ifstream cFile;
        std::string line;
        cFile.open("config.csv");
        for (int i = 0; i < (LINE+1); ++i) { std::getline (cFile, line); }
        std::getline (cFile, line);
        std::cout << line << std::endl;

        std::stringstream ss (line);
        std::string word;
        std::getline (ss, word, ',');
        const int numEleUp = std::stoi (word);
        std::getline (ss, word, ',');
        const int numEleDown = std::stoi (word);
        std::getline (ss, word, ',');
        const int numSiteX = std::stoi (word);
        std::getline (ss, word, ',');
        const int numSiteY = std::stoi (word);
        std::getline (ss, word, ',');
        const std::string bouConX = word;
        std::getline (ss, word, ',');
        const std::string bouConY = word;

        const int numSam = 1;
        const int numEval = 5;

        const double PI = std::acos (-1.0);
        const std::complex<double> unitI (0.0, 1.0);

        std::cout << numEleUp << " " << numEleDown << " " << numSiteX << " " << numSiteY << " " << bouConX << " " << bouConY << std::endl;

        time_t start, end;
        start = time (NULL);

        double U = 2.0;
        // double step = U;

        std::ofstream file_log ("log", std::ios_base::app);
        std::ofstream file_eigvals ("eigenvalues.dat", std::ios_base::app | std::ios_base::binary);
        std::ofstream file_eigvecs ("eigenvectors.dat", std::ios_base::app | std::ios_base::binary);
        std::ofstream file_data ("results.dat", std::ios_base::app);

        file_data << "u," << "gsE," <<  "transv," << "szCor" << std::endl;

        for (int s = 0; s < numSam; ++s) {
            Hubbard<std::complex<double>> Hub (U, numEleUp, numEleDown, numSiteX, numSiteY, bouConX, bouConY);
            // std::complex<double> gauge = std::exp (unitI*(2.0*PI*s / numSam));
            // std::complex<double> gauge = std::exp (unitI*0.5*PI);
            std::complex<double> gauge = std::exp (unitI*1.0*PI);
            // Hub.SetLink (1, 3, gauge);
            // Hub.SetLink (0, 1, gauge);
            // Hub.SetLink (3, 4, gauge);
            // Hub.SetLink (5, 9, gauge);
            // Hub.SetLink (7, 8, gauge);
            // Hub.SetLink (9, 10, gauge);
            time_t conStart = time (NULL);
            Hub.CSCHamMatrix ();
            time_t conEnd = time (NULL);
            // std::cout << "Construction time: " << (conEnd-conStart) << " s" << std::endl;
            int dim = Hub.Dim ();
            // Hub.PrintHam ();
            std::cout << "Dim " << dim << std::endl;
            ARCompStdEig<double, Hubbard<std::complex<double>>> EigProb;
            EigProb.DefineParameters (dim, numEval, &Hub, &Hubbard<std::complex<double>>::Mv, "SR", numEval*10);
            // EigProb.DefineParameters (dim, numEval, &Hub, &Hubbard<std::complex<double>>::Mv, "SR");
            auto eigVal = new std::complex<double> [numEval];
            auto eigVec = new std::complex<double> [numEval*dim];
            int nconv = EigProb.EigenValVectors (eigVec, eigVal);
            // To sort the eigenvalues and keep track of the index.
            std::vector<std::pair<double, int>> temp;
            for (int j = 0; j < nconv; ++j) { temp.push_back (std::pair<double, int> (std::real (eigVal[j]), j)); }
            std::sort (temp.begin (), temp.end ());
            for (int j = 0; j < nconv; ++j) {
                double val = temp[j].first;
                int index = temp[j].second;
                file_eigvals.write ((char*)(&val), sizeof (double));
                for (int k = 0; k < dim; ++k) {
                    int d = index*dim+k;
                    double rr = std::real (eigVec[d]);
                    double ii = std::imag (eigVec[d]);
                    file_eigvecs.write ((char*)(&rr), sizeof (double));
                    file_eigvecs.write ((char*)(&ii), sizeof (double));
                    }
                }
            std::cout << s << " " << gauge << std::endl;
            if (s < 10) {
                for (int j = 0; j < numEval; ++j) { std::cout << std::setprecision (12) << eigVal[temp[j].second] << std::endl; }
                // for (int j = 0; j < numEval; ++j) { std::cout << std::setprecision (12) << temp[j].first << std::endl; }
                }
            // file_data << std::setprecision (4) << U << "," << std::setprecision (8) << temp[0].first << std::endl;
            // U += step;
            delete [] eigVal;
            delete [] eigVec;
            }
        end = time (NULL);
        file_log << "Time: " << (end-start)/60.0 << " min" << std::endl;
        std::cout << "Total time: " << (end-start) << " s" << std::endl;
        file_eigvals.close ();
        file_eigvecs.close ();
        file_log.close ();
        return 1;
    }
