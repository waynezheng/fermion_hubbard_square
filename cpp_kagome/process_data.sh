 #!/bin/bash

X=06
Y=01
UE=03
DE=02
U=0.0
SPEC=10

NUMSAM=1000

 mv eigenvalues.dat eigenvalues${SPEC}_lattice${X}${Y}PO_u${U}_filling${UE}${DE}_phiNum${NUMSAM}.dat 
 mv eigenvectors.dat eigenvectors${SPEC}_lattice${X}${Y}PO_u${U}_filling${UE}${DE}_phiNum${NUMSAM}.dat 
 # mv hilbert_space.dat hilbert_space_lattice${X}${Y}PO_u${U}_filling${UE}${DE}.txt
 # mv hilbert_space.dat hilbert_space_lattice${X}${Y}PO_u${U}_filling${UE}${DE}.txt

 mv *.dat /Users/wayne/Downloads/data/hubbard

