// A class is an object. Here the object Hubbard denotes a quantum system consisting of a specific lattce and kind of Hamiltoian.i
typedef long long int int64;

template<typename T> class Hubbard {
        private:
        // model paramaters
        double u_; // Hubbard U. t=1.0 is set as energy unit
        int64 num_up_; // spin-up electron #
        int64 num_down_; // spin-down electron #
        // define the lattice
        int64 numsite_x_;
        int64 numsite_y_;
        int64 numsite_;
        std::string boundary_x_;
        std::string boundary_y_;
        // dimension and basis of the Hilbert
        int64 dim_;
        std::vector<int64> basis_;
        std::vector<int64> sites_;
        std::vector<std::vector<int64>> sites_forward_;
        std::vector<T> hoppings_;

        int64 num_nzero_; // # of non-zero matrix elements
        std::vector<T> ham_matrix_;
        std::vector<int64> index_row_; // row index
        std::vector<int64> index_col_; // column index

        public:
        Hubbard (const double u,
                 const int64 numup, const int64 numdown,
                 const int64 numsite_x, const int64 numsite_y,
                 const std::string boundary_x, const std::string boundary_y);
        ~Hubbard ();
        
        int64 Dim ();
        void SetNegativeLink (int64 i, int64 z);
        void SetLink (int64 i, int64 j, int64 z, T a);
        void CSCHamMatrix ();
        inline void MultiVec (T* v, T* w);
        void TotalSMinusSPlus (T* v, T* w);
        void TotalSPlusSMinus (T* v, T* w);
        void SZSZ (T* v, T* w, int64 i, int64 j);

        void SaveHilbert ();
        void SetOne (T* v, int64 i);
        T Dot (T* v, T* w);
        void PrintHam ();
        };

// to define a specific quantum system and build up the corresponding Hilbert space in the tensor product representation
template<typename T>
Hubbard<T>::Hubbard (const double u,
                     const int64 numup, const int64 numdown,
                     const int64 numsite_x, const int64 numsite_y,
                     const std::string boundary_x, const std::string boundary_y) {
        u_ = u;
        num_up_ = numup;
        num_down_ = numdown;
        numsite_x_ = numsite_x;
        numsite_y_ = numsite_y;
        numsite_ = numsite_x_*numsite_y_;
        boundary_x_ = boundary_x;
        boundary_y_ = boundary_y;
        // construct the basis of the Hilbert space constrained by the U(1) symmetry
        int64 max = int64 (pow (2, numsite_));
        int64 dim = 0;
        for (int64 i = 0; i < max; ++i) {
            boost::dynamic_bitset<> bit_up (numsite_, i);
            if (int64(bit_up.count()) == numup) {
                for (int64 j = 0; j < max; ++j) {
                    boost::dynamic_bitset<> bit_down (numsite_, j);
                    if (int64(bit_down.count()) == numdown) {
                        boost::dynamic_bitset<> bit;
                        for (int64 k = 0; k < numsite_; ++k) { bit.push_back (bit_up[k]); } // basis of tensor product of up- and down-spin
                        for (int64 k = 0; k < numsite_; ++k) { bit.push_back (bit_down[k]); }
                        basis_.push_back (bit.to_ulong ());
                        ++dim;
                        }
                    }
                }
            }
        std::sort (basis_.begin (), basis_.end ());
        dim_ = dim;
        // initialize hopping amplitude
        for (int64 j = 0; j < numsite_y_; ++j) {
            for (int64 i = 0; i < numsite_x_; ++i) {
                sites_.push_back(j*numsite_x_+i);
                std::vector<int64> temp;
                temp.push_back (j*numsite_x_+((i+1) % numsite_x_));
                temp.push_back (((j+1) % numsite_y_)*numsite_x_+i);
                sites_forward_.push_back (temp);
                hoppings_.push_back (1.0);
                hoppings_.push_back (1.0);
                }
            }
        }

template<typename T> Hubbard<T>::~Hubbard () {}

template<typename T> int64 Hubbard<T>::Dim () { return dim_; }

template<typename T> void Hubbard<T>::SetLink (int64 i, int64 j, int64 z, T a) {
        int64 k = j*numsite_x_+i;
        hoppings_[k*2+z] = a;
        }

// construct the sparse matrix
template<typename T> void Hubbard<T>::CSCHamMatrix () {
        int64 count = 0;
        for (int64 l = 0; l < dim_; ++l) {
            int64 b = basis_[l];
            boost::dynamic_bitset<> config (numsite_*2, b);
            std::map<int64, T, std::less<int64>> map_col; // sorted map in asending order
            for (int64 current_site = 0; current_site < numsite_; ++current_site) { // current site
                // Hubbard U
                if (config[current_site] & config[numsite_+current_site]) {
                    auto it_u = map_col.find (l);
                    if (it_u == map_col.end ()) { map_col.insert (std::make_pair (l, u_)); }
                    else { it_u->second += u_; }
                    }
                // hoppings
                for (int64 spin = 0; spin < 2; ++spin) {
                    // move forward
                    for (int64 move = 0; move < 2; ++move) {
                        int64 forward_site = sites_forward_[current_site][move];
                        int64 current_bit = spin*numsite_ + current_site;
                        int64 forward_bit = spin*numsite_ + forward_site;
                        if ((config[current_bit] ^ config[forward_bit])) {
                            T phase = 0.0;
                            if (1 == config[current_bit]) { phase = hoppings_[current_site*2 + move]; }
                            else { phase = std::conj (hoppings_[current_site*2+move]); }
                            double fermion_sgn = 1.0;
                            int64 low = current_bit;
                            int64 high = forward_bit;
                            if (forward_bit < current_bit) {
                                low = forward_bit;
                                high = current_bit;
                                }
                            for (int64 m = low+1; m < high; ++m) { if (config[m]) { fermion_sgn *= -1.0; } }
                            boost::dynamic_bitset<> temp (config);
                            temp.flip (current_bit);
                            temp.flip (forward_bit);
                            std::vector<int64>::iterator it_flip = std::lower_bound (basis_.begin (), basis_.end (), int64 (temp.to_ulong ()));
                            int64 k = std::distance (basis_.begin (), it_flip);
                            auto it_col = map_col.find (k);
                            if (forward_site > current_site) {
                                if (it_col == map_col.end ()) { map_col.insert (std::make_pair (k, -1.0*phase*fermion_sgn)); }
                                else { it_col->second += -1.0*phase*fermion_sgn; }
                                }
                            // deal with boundary conditions
                            else if ((boundary_x_ == "PBC") && ((numsite_x_ - 1) == (current_site - forward_site))) {
                                if (it_col == map_col.end ()) { map_col.insert (std::make_pair (k, -1.0*phase*fermion_sgn)); }
                                else { it_col->second += -1.0*phase*fermion_sgn; }
                              }
                            else if ((boundary_y_ == "PBC") && ((numsite_y_ - 1)*numsite_x_ == (current_site - forward_site))) {
                                if (it_col == map_col.end ()) { map_col.insert (std::make_pair (k, -1.0*phase*fermion_sgn)); }
                                else { it_col->second += -1.0*phase*fermion_sgn; }
                                }
                            }
                        }
                    }
                }
            for (auto it = map_col.begin (); it != map_col.end (); ++it) {
                index_col_.push_back (l);
                index_row_.push_back (it->first);
                ham_matrix_.push_back (it->second);
                ++count;
                }
            }
        num_nzero_ = count;
        }

// Required by Arpack++ package handbook:
// There only requirements make by ARPACK++ are that member funtion Hamiltonian() musth have two pointers to vectors of type T as paraments and the input vector must precede the output vector.
template<typename T>
inline void Hubbard<T>::MultiVec (T* v, T* w) {
        std::fill_n (w, dim_, 0.0);
        for (int64 l = 0; l < num_nzero_; ++l) { w[index_row_[l]] += ham_matrix_[l]*v[index_col_[l]]; }
        }

template<typename T>
void Hubbard<T>::TotalSMinusSPlus (T* v, T* w) {
        std::fill_n (w, dim_, 0.0);
        for (int64 l = 0; l < dim_; ++l) {
            if (0.0 == v[l]) { continue; }
            int64 b = basis_[l];
            boost::dynamic_bitset<> config(numsite_*2, b);
            for (int64 i = 0; i < numsite_; ++i) {
                for (int64 j = 0; j < numsite_; ++j) {
                    boost::dynamic_bitset<> temp(config);
                    if (i == j) {
                        w[l] += double(config[numsite_+j]-config[j]*config[numsite_+j])*v[l];
                        }
                    else {
                        int64 upI = i;
                        int64 upJ = j;
                        int64 downI = numsite_+i;
                        int64 downJ = numsite_+j;
                        if (1 == config[upI] && 0 == config[upJ] && 1 == config[downJ] && 0 == config[downI]) {
                            int64 upCross = 0;
                            int64 downCross = 0;
                            if (i < j) {
                                for (int64 m = upI+1; m < upJ; ++m) { if (config[m]) { ++upCross; } }
                                for (int64 m = downJ-1; m > downI; --m) { if (config[m]) { ++downCross; } }
                                }
                            else {
                                for (int64 m = upI-1; m > upJ; --m) { if (config[m]) { ++upCross; } }
                                for (int64 m = downJ+1; m < downI; ++m) { if (config[m]) { ++downCross; } }
                                }
                            double fermion_sgn = pow(-1.0, upCross+downCross);
                            temp.flip(upI);
                            temp.flip(upJ);
                            temp.flip(downI);
                            temp.flip(downJ);
                            std::vector<int64>::iterator it = std::lower_bound(basis_.begin(), basis_.end(), int64(temp.to_ulong()));
                            w[std::distance(basis_.begin(), it)] += -1.0*fermion_sgn*v[l];
                            }
                        }
                    }
                }
            }
        }

template<typename T>
void Hubbard<T>::TotalSPlusSMinus (T* v, T* w) {
        std::fill_n(w, dim_, 0.0);
        for (int64 l = 0; l < dim_; ++l) {
            if (0.0 == v[l]) { continue; }
            int64 b = basis_[l];
            boost::dynamic_bitset<> config(numsite_*2, b);
            for (int64 i = 0; i < numsite_; ++i) {
                for (int64 j = 0; j < numsite_; ++j) {
                    boost::dynamic_bitset<> temp(config);
                    if (i == j) {
                        w[l] += double(config[j]-config[j]*config[numsite_+j])*v[l];
                        }
                    else {
                        int64 upI = i;
                        int64 upJ = j;
                        int64 downI = numsite_+i;
                        int64 downJ = numsite_+j;
                        if (1 == config[upJ] && 0 == config[upI] && 1 == config[downI] && 0 == config[downJ]) {
                            int64 upCross = 0;
                            int64 downCross = 0;
                            if (i < j) {
                                for (int64 m = upJ-1; m > upI; --m) { if (config[m]) { ++upCross; } }
                                for (int64 m = downI+1; m < downJ; ++m) { if (config[m]) { ++downCross; } }
                                }
                            else {
                                for (int64 m = upJ+1; m < upI; ++m) { if (config[m]) { ++upCross; } }
                                for (int64 m = downI-1; m > downJ; --m) { if (config[m]) { ++downCross; } }
                                }
                            double fermion_sgn = pow(-1.0, upCross+downCross);
                            temp.flip(upI);
                            temp.flip(upJ);
                            temp.flip(downI);
                            temp.flip(downJ);
                            std::vector<int64>::iterator it = std::lower_bound(basis_.begin(), basis_.end(), int64(temp.to_ulong()));
                            w[std::distance(basis_.begin(), it)] += -1.0*fermion_sgn*v[l];
                            }
                        }
                    }
                }
            }
        }

template<typename T>
void Hubbard<T>::SZSZ(T* v, T* w, int64 i, int64 j) {
        std::fill_n(w, dim_, 0.0);
        for (int64 l = 0; l < dim_; ++l) {
            if (0.0 == v[l]) { continue; }
            int64 b = basis_[l];
            boost::dynamic_bitset<> config (numsite_*2, b);
            w[l] += 0.25*(config[i]-config[numsite_+i])*(config[j]-config[numsite_+j])*v[l];
            }
        }

template<typename T>
void Hubbard<T>::SaveHilbert() {
        std::ofstream file_hilbert("hilbert_space.dat", std::ios_base::app);
        for (int64 l = 0; l < dim_; ++l) {
            file_hilbert << basis_[l];
            if (l != (dim_-1)) { file_hilbert << ","; }
            }
        file_hilbert.close();
        }

template<typename T>
void Hubbard<T>::SetOne(T* v, int64 i) {
        std::fill_n(v, dim_, 0.0);
        v[i] = 1.0;
        }

template<typename T>
T Hubbard<T>::Dot(T* v, T* w) {
        T r = 0.0;
        for (int64 l = 0; l < dim_; ++l) { r += std::conj(v[l])*w[l]; }
        return r;
        }

template<typename T>
void Hubbard<T>::PrintHam () {
        std::ofstream file_ham("ham.dat", std::ios_base::app | std::ios_base::binary);
        auto u = new T[dim_];
        auto v = new T[dim_];
        auto w = new T[dim_];
        for (int64 i = 0; i < dim_; ++i) {
            for (int j = 0; j < dim_; ++j) {
                SetOne(v, i);
                SetOne(w, j);
                Hamiltonian(w, u);
                // std::cout << std::real(Dot(v, u)) << ", ";
                double r = std::real(Dot(v, u));
                file_ham.write((char*)(&r), sizeof(double));
                }
            // std::cout << std::endl;
            }
        // std::cout << std::endl;
        file_ham.close();
        delete [] u;
        delete [] v;
        delete [] w;
        }
