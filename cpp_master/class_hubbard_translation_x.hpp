#include "class_translation_rep.hpp"
typedef long long int int64;
class TransRepX;

// A class is an object. Here the object Hubbard denotes a quantum system consisting of a specific lattce and kind of Hamiltoian.i
template<typename T>
class Hubbard {
        private:
        // model paramaters
        double u_; // Hubbard U, t=1.0 is set as energy unit
        int64 num_up_; // spin-up electron #
        int64 num_down_; // spin-down electron #
        // define the lattice
        int64 numsite_x_;
        int64 numsite_y_;
        int64 numsite_;
        std::string boundary_x_;
        std::string boundary_y_;
        // dimension and basis of the Hilbert space
        int64 dim_;
        std::vector<int64> basis_;
        std::vector<int> site_;
        std::vector<std::vector<int>> site_forward_;
        std::vector<T> hoppings_;
        // translational momenta
        double kx_;
        double ky_;
        // reduced Hilbert space
        std::vector<std::pair<int64, double>> basis_x_;
        std::vector<TransRepX> basis_x_tag_;
        int64 dim_x_;
        int64 num_nzero_elements_x_; // # of non-zero matrix elements
        std::vector<T> matrix_ham_x;
        std::vector<int> row_index_x; // row index
        std::vector<int> colum_index_x; // column index

        public:
        Hubbard (const double u,
                 const int64 num_up, const int64 num_down,
                 const int64 num_site_x, const int64 num_site_y,
                 const std::string boundary_x, const std::string boundary_y,
                 const int64 nx, const int64 ny);
        ~Hubbard (); // Destructor.

        int64 Dim ();
        int64 Coordinate (const int64 x, const int64 y, const int64 s);
        int64 Permutation (std::vector<int>& seq);
        void SetNegativeLink (int64 i, int64 z);
        void SetLink (int64 i, int64 j, int64 z, T a);
        T Dot (T* v, T* w);
        int64 TranslationX (const int64 b);
        void TranslationXBasis ();
        void CSCHamMatrixTranslationX ();
        inline void MultiVec (T* v, T* w);

        void HoppingXIJ (int64 y, int64 spin, T* v, T* w);
        void HoppingXJI (int64 y, int64 spin, T* v, T* w);
        };

// for translational symmetry
template<typename T>
Hubbard<T>::Hubbard (const double u,
                     const int64 num_up, const int64 num_down,
                     const int64 num_site_x, const int64 num_site_y,
                     const std::string boundary_x, const std::string boundary_y,
                     const int64 nx, const int64 ny) {
        u_ = u;
        num_up_ = num_up;
        num_down_ = num_down;
        numsite_x_ = num_site_x;
        numsite_y_ = num_site_y;
        numsite_ = numsite_x_*numsite_y_;
        boundary_x_ = boundary_x;
        boundary_y_ = boundary_y;
        kx_ = 2.0*PI*nx / numsite_x_;
        ky_ = 2.0*PI*ny / numsite_y_;
        // Construct the basis of the Hilbert space constrained by the U(1) symmetry.
        int64 max = int64 (std::pow (2, numsite_));
        int64 dim = 0;
        for (int64 i = 0; i < max; ++i) {
            boost::dynamic_bitset<> bit_up (numsite_, i);
            if (int64 (bit_up.count ()) == num_up_) {
                for (int64 j = 0; j < max; ++j) {
                    boost::dynamic_bitset<> bit_down (numsite_, j);
                    if (int(bit_down.count ()) == num_down_) {
                        boost::dynamic_bitset<> bit;
                        for (int64 k = 0; k < numsite_; ++k) { bit.push_back (bit_up[k]); } // basis of tensor product of up- and down-spins
                        for (int64 k = 0; k < numsite_; ++k) { bit.push_back (bit_down[k]); }
                        basis_.push_back (bit.to_ulong ());
                        ++dim;
                        }
                    }
                }
            }
        std::sort (basis_.begin (), basis_.end ());
        dim_ = dim;
        // initialize hopping amplitude
        for (int64 j = 0; j < numsite_y_; ++j) {
            for (int64 i = 0; i < numsite_x_; ++i) {
                site_.push_back (j*numsite_x_ + i);
                std::vector<int> temp;
                temp.push_back (j*numsite_x_ + ((i+1) % numsite_x_));
                temp.push_back (((j+1) % numsite_y_)*numsite_x_ + i);
                site_forward_.push_back (temp);
                hoppings_.push_back (1.0);
                hoppings_.push_back (1.0);
                }
            }
        }

template<typename T>
Hubbard<T>::~Hubbard () {}

template<typename T>
int64 Hubbard<T>::Coordinate (const int64 x, const int64 y, const int64 s) { return s*numsite_+y*numsite_x_+x; }

// determine the fermion sign for a permutation
template<typename T>
int64 Hubbard<T>::Permutation (std::vector<int> &seq) {
        const int64 length = seq.size ();
        int64 count = 0;
        for (int64 i = 0; i < (length-1); ++i) {
            for (int64 j = 0; j < (length-1-i); ++j) {
                if (seq[j] > seq[j+1]) {
                    std::swap (seq[j], seq[j+1]);
                    ++count;
                    }
                }
            }
        return count;
        }

template<typename T>
int64 Hubbard<T>::TranslationX (const int64 b) {
        boost::dynamic_bitset<> config (numsite_*2, b);
        boost::dynamic_bitset<> temp (numsite_*2);
        std::vector<int> order;
        for (int64 spin = 0; spin < 2; ++spin) {
            for (int64 j = 0; j < numsite_y_; ++j) {
                for (int64 i = 0; i < numsite_x_; ++i) {
                    int64 k = Coordinate (i, j, spin);
                    if (config[k]) {
                        int64 kk = Coordinate ((i+1) % numsite_x_, j, spin);
                        order.push_back (kk);
                        temp[kk] = 1;
                        }
                    }
                }
            }
        int64 num = Permutation (order);
        int64 sgn = 1;
        if (num & 1) { sgn = -1; } // faster than "num % 2"
        auto it = std::lower_bound (basis_.begin (), basis_.end (), temp.to_ulong ());
        return sgn*basis_[std::distance (basis_.begin (), it)];
        }

template<typename T>
void Hubbard<T>::TranslationXBasis () {
        TransRepX null_rep (-1, -1, 0.0);
        basis_x_tag_ = std::vector<TransRepX> (dim_, null_rep);
        std::vector<bool> mark (dim_, true);
        for (auto it = basis_.begin (); it != basis_.end (); ++it) {
            int64 i = std::distance (basis_.begin (), it);
            if (mark[i]) {
                std::vector<int64> seq_val; // sequence of values
                std::vector<double> seq_sgn;
                int64 ref = *it;
                int64 val = std::abs (ref);
                double sgn = 1.0; // trace the sign accmulated during one period
                seq_val.push_back (val);
                seq_sgn.push_back (sgn);
                mark[i] = false;
                int64 cfg = TranslationX (val);
                val = std::abs (cfg);
                if (cfg < 0) { sgn *= -1.0; }
                while (val != *it) {
                    seq_val.push_back (val);
                    seq_sgn.push_back (sgn);
                    cfg = TranslationX (val);
                    auto it_val = std::lower_bound (basis_.begin (), basis_.end (), val);
                    mark[std::distance (basis_.begin (), it_val)] = false;
                    val = std::abs (cfg);
                    if (cfg < 0) { sgn *= -1.0; }
                    }
                // here val is already translated to ref, sgn is the sign of a period
                int64 period = seq_val.size ();
                int64 num_period = int64 (numsite_x_ / period);
                std::complex<double> gam = 0.0;
                for (int64 l = 0; l < num_period; ++l) { gam += std::exp (-1.0*IMAG_I*kx_*double (period*l)) * std::pow (sgn, l); }
                double gam_square = std::real (gam*std::conj (gam));
                if (gam_square > 1e-15) {
                    basis_x_.push_back (std::make_pair (seq_val.front (), period*gam_square));
                    int64 idx = std::distance (basis_x_.begin (), basis_x_.end () - 1);
                    for (auto it_seq = seq_val.begin (); it_seq != seq_val.end (); ++it_seq) {
                        int64 position = std::distance (seq_val.begin (), it_seq);
                        TransRepX tag (idx, position, seq_sgn[position]);
                        auto it_val = std::lower_bound (basis_.begin (), basis_.end (), *it_seq);
                        basis_x_tag_[std::distance (basis_.begin (), it_val)] = tag;
                        }
                    }
                }
            }
        dim_x_ = basis_x_.size ();
        }

// construct the sparse matrix
template<typename T>
void Hubbard<T>::CSCHamMatrixTranslationX () {
        int64 count = 0;
        for (auto it = basis_x_.begin (); it != basis_x_.end (); ++it) {
            int64 l = std::distance (basis_x_.begin (), it);
            boost::dynamic_bitset<> config (numsite_*2, it->first);
            std::map<int, T, std::less<int>> map_col; // sorted map in an asending order
            for (int64 current_site = 0; current_site < numsite_; ++current_site) {
                // Hubbard U
                if (config[current_site] & config[numsite_+current_site]) {
                    auto it_u = map_col.find (l);
                    if (it_u == map_col.end ()) { map_col.insert (std::make_pair (l, u_)); }
                    else { it_u->second += u_; }
                    }
                // hoppings
                for (int64 spin = 0; spin < 2; ++spin) {
                    // move forward to x- and y-direction
                    for (int64 move = 0; move < 2; ++move) {
                        int64 forward_site = site_forward_[current_site][move];
                        // std::cout << current_site << " " << forward_site << std::endl;
                        int64 current_bit = spin*numsite_ + current_site;
                        int64 forward_bit = spin*numsite_ + forward_site;
                        if ((config[current_bit] ^ config[forward_bit])) {
                            T phase = 0.0;
                            if (1 == config[current_bit]) { phase = hoppings_[current_site*2 + move]; }
                            else { phase = std::conj (hoppings_[current_site*2+move]); }
                            int low = current_bit;
                            int high = forward_bit;
                            if (forward_site < current_site) {
                                low = forward_bit;
                                high = current_bit;
                                }
                            double fermion_sgn = 1.0;
                            for (int64 m = low+1; m < high; ++m) { if (config[m]) { fermion_sgn *= -1.0; } }
                            boost::dynamic_bitset<> temp (config);
                            temp.flip (current_bit);
                            temp.flip (forward_bit);
                            auto it_flipped = std::lower_bound (basis_.begin (), basis_.end (), int64 (temp.to_ulong ()));
                            auto tag_flipped = basis_x_tag_[std::distance (basis_.begin (), it_flipped)];
                            int64 k = tag_flipped.Index ();
                            int64 translation = tag_flipped.Position ();
                            double translation_sgn = tag_flipped.Sign ();
                            std::complex<double> ele = -1.0*phase*fermion_sgn;
                            ele *= translation_sgn*std::exp (-1.0*IMAG_I*(kx_*translation)) * std::sqrt (basis_x_[k].second / basis_x_[l].second);
                            auto it_col = map_col.find (k);
                            if (forward_site > current_site) {
                                if (it_col == map_col.end ()) { map_col.insert (std::make_pair (k, ele)); }
                                else { it_col->second += ele; }
                                }
                            // deal with boundary conditions
                            else if ((boundary_x_ == "PBC") && ((numsite_x_-1) == (current_site-forward_site))) {
                                if (it_col == map_col.end ()) { map_col.insert (std::make_pair (k, ele)); }
                                else { it_col->second += ele; }
                                }
                            else if ((boundary_y_ == "PBC") && ((numsite_y_-1)*numsite_x_ == (current_site-forward_site))) {
                                // std::cout << config << std::endl;
                                // std::cout << current_site << " " << forward_site << "-" <<current_bit << " " << forward_bit << std::endl;
                                if (it_col == map_col.end ()) { map_col.insert (std::make_pair (k, ele)); }
                                else { it_col->second += ele; }
                               }
                            }
                        }
                    }
                }
            for (auto it_col = map_col.begin (); it_col != map_col.end (); ++it_col) {
                colum_index_x.push_back (l);
                row_index_x.push_back (it_col->first);
                matrix_ham_x.push_back (it_col->second);
                ++count;
                }
            }
        num_nzero_elements_x_ = count;
        // std::cout << num_nzero_elements_x_ << std::endl;
        }

template<typename T>
inline void Hubbard<T>::MultiVec (T* v, T* w) {
        std::fill_n (w, dim_x_, 0.0);
        for (int64 l = 0; l < num_nzero_elements_x_; ++l) { w[row_index_x[l]] += matrix_ham_x[l]*v[colum_index_x[l]]; }
        }

template<typename T>
int64 Hubbard<T>::Dim () { return dim_x_; }

template<typename T>
void Hubbard<T>::SetNegativeLink (int64 i, int64 z) { hoppings_[i*2+z] = -1.0; }

template<typename T>
void Hubbard<T>::SetLink (int64 i, int64 j, int64 z, T a) {
        int64 k = j*numsite_x_+i;
        hoppings_[k*2+z] = a;
        }

template<typename T>
T Hubbard<T>::Dot (T* v, T* w) {
        T res = 0.0;
        for (auto it = basis_x_.begin (); it != basis_x_. end (); ++it) {
            int64 l = std::distance (basis_x_. begin(), it);
            res += v[l]*std::conj (w[l]);
            }
        return res;
        }

template<typename T>
void Hubbard<T>::HoppingXIJ (int64 y, int64 spin, T* v, T* w) {
        std::fill_n (w, dim_x_, 0.0);
        for (auto it = basis_x_.begin (); it != basis_x_.end (); ++it) {
            int64 l = std::distance (basis_x_.begin (), it);
            int64 a = it->first;
            boost::dynamic_bitset<> config (numsite_*2, a);
            for (int64 s = 0; s < numsite_x_; ++s) {
                int64 i = spin*numsite_+y*numsite_x_+s;
                int64 j = spin*numsite_+y*numsite_x_+((s+1) % numsite_x_);
                if (0 == config[i] && 1 == config[j]) {
                    double fermionSign = 1.0;
                    for (int64 m = std::min (i, j)+1; m < std::max (i, j); ++m) { if (config[m]) { fermionSign = -1.0*fermionSign; } }
                    boost::dynamic_bitset<> temp (config);
                    temp.flip (i);
                    temp.flip (j);
                    auto itt = std::lower_bound (basis_.begin (), basis_.end (), int64 (temp.to_ulong ()));
                    auto rep = basis_x_tag_[std::distance (basis_.begin (), itt)];
                    int64 k = rep.Index ();
                    int64 pos = rep.Position ();
                    double sign = rep.Sign ();
                    w[k] += fermionSign*sign*std::exp (-1.0*IMAG_I*(kx_*pos))*std::sqrt (basis_x_[k].second / basis_x_[l].second)*v[l];
                    }
                }
            }
        }

template<typename T>
void Hubbard<T>::HoppingXJI (int64 y, int64 spin, T* v, T* w) {
        std::fill_n (w, dim_x_, 0.0);
        for (auto it = basis_x_.begin (); it != basis_x_.end (); ++it) {
            int64 l = std::distance (basis_x_.begin (), it);
            int64 a = it->first;
            boost::dynamic_bitset<> config (numsite_*2, a);
            for (int64 s = 0; s < numsite_x_; ++s) {
                int64 i = spin*numsite_+y*numsite_x_+s;
                int64 j = spin*numsite_+y*numsite_x_+((s+1) % numsite_x_);
                if (0 == config[j] && 1 == config[i]) {
                    double fermionSign = 1.0;
                    for (int64 m = std::min (i, j)+1; m < std::max (i, j); ++m) { if (config[m]) { fermionSign = -1.0*fermionSign; } }
                    boost::dynamic_bitset<> temp (config);
                    temp.flip (i);
                    temp.flip (j);
                    auto itt = std::lower_bound (basis_.begin (), basis_.end (), int64 (temp.to_ulong ()));
                    auto rep = basis_x_tag_[std::distance (basis_.begin (), itt)];
                    int64 k = rep.Index ();
                    int64 pos = rep.Position ();
                    double sign = rep.Sign ();
                    w[k] += fermionSign*sign*std::exp (-1.0*IMAG_I*(kx_*pos))*std::sqrt (basis_x_[k].second / basis_x_[l].second)*v[l];
                    }
                }
            }
        }
