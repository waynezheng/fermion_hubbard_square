#include "class_translation_rep.hpp"
typedef long long int int64;
class TranslationTag; // class forward declartion

// A class is an object. Here the object Hubbard denotes a quantum system consisting of a specific lattce and kind of Hamiltoian.i
template<typename T>
class Hubbard {
        private:
        // model paramaters
        double u_; // Hubbard U, t=1.0 is set as energy unit
        int64 numup_; // spin-up electrons #
        int64 numdown_; // spin-down electrons #
        // define the lattice
        int64 numsite_x_;
        int64 numsite_y_;
        int64 numsite_;
        std::string boundary_x_;
        std::string boundary_y_;
        // dimension and basis of the original Hilbert space
        int64 dim_;
        std::vector<int64> basis_;
        std::vector<int64> sites_;
        std::vector<std::vector<int64>> sites_forward_;
        std::vector<T> hoppings_;

        // define the momentum sector
        double kx_; 
        double ky_;

        std::vector<std::pair<int64, double>> basis_xy_;
        std::vector<TranslationTag> tag_basis_xy_;

        int64 dim_xy_;
        int64 num_nzero_xy_; // # of non-zero matrix elements
        std::vector<T> ham_matrix_xy_;
        std::vector<int64> index_row_xy_; // index of row
        std::vector<int64> index_col_xy_; // index of column

        public:
        int64 Coordinate (const int64 x, const int64 y, const int64 s);
        Hubbard (double u, int64 numup, int64 numdown, int64 numsite_x, int64 numsite_y, std::string boundary_x, std::string boundary_y, int64 nx, int64 ny);
        ~Hubbard ();
        
        int64 Dim ();
        int64 Permutation (std::vector<int64> &seq);
        void SetLink (const int64 i, const int64 j, const int64 z, const T a);
        // void SetNegativeLink (int64 i, int64 z);

        T Dot (const T* v, const T* w);
        int64 TranslationX (const int64 b);
        int64 TranslationY (const int64 b);
        void TranslationXYBasis ();
        int64 DimXY ();
        void CSCHamiltonianXY ();
        void MultiVecXY (T* v, T* w);
        
        // void HoppingXIJ (int y, int64 spin, T* v, T* w);
        // void HoppingXJI (int64 y, int64 spin, T* v, T* w);
        };

template<typename T>
int64 Hubbard<T>::Coordinate (const int64 x, const int64 y, const int64 s) { return s*numsite_+y*numsite_x_+x; }

template<typename T>
Hubbard<T>::Hubbard (double u, int64 numup, int64 numdown, int64 numsite_x, int64 numsite_y, std::string boundary_x, std::string boundary_y, int64 nx, int64 ny) {
        u_ = u;
        numup_ = numup;
        numdown_ = numdown;
        numsite_x_ = numsite_x;
        numsite_y_ = numsite_y;
        numsite_ = numsite_x_*numsite_y_;
        boundary_x_ = boundary_x;
        boundary_y_ = boundary_y;
        kx_ = 2.0*PI*nx / numsite_x_;
        ky_ = 2.0*PI*ny / numsite_y_;
        // "int64" is 64 bits, in another word, it can be used to store a 32 sites lattcie for Hubbard model
        // construct the basis of the Hilbert space constrained by the U(1) symmetry
        int64 max = int64 (std::pow (2, numsite_)); // mNumsite should be less than 32
        for (int64 i = 0; i < max; ++i) {
            boost::dynamic_bitset<> bit_up (numsite_, i);
            if (bit_up.count () == numup_) {
                for (int64 j = 0; j < max; ++j) {
                    boost::dynamic_bitset<> bit_down (numsite_, j);
                    if (bit_down.count () == numdown_) {
                        boost::dynamic_bitset<> bit;
                        for (int64 k = 0; k < numsite_; ++k) { bit.push_back (bit_up[k]); } // Basis of tensor product of up- and down-spins.
                        for (int64 k = 0; k < numsite_; ++k) { bit.push_back (bit_down[k]); }
                        // dynamic_bitset<>::to_ulong () actually can be used as to_ullong (), which exceeds 32 bits
                        basis_.push_back (bit.to_ulong ());
                        }
                    }
                }
            }
        std::sort (basis_.begin (), basis_.end ());
        dim_ = basis_.size ();
        // initialize hopping amplitude
        for (int64 j = 0; j < numsite_y_; ++j) {
            for (int64 i = 0; i < numsite_x_; ++i) {
                sites_.push_back (j*numsite_x_+i);
                std::vector<int64> temp;
                temp.push_back (j*numsite_x_+((i+1) % numsite_x_));
                temp.push_back (((j+1) % numsite_y_)*numsite_x_+i);
                sites_forward_.push_back (temp);
                hoppings_.push_back (1.0);
                hoppings_.push_back (1.0);
                }
            }
        }

// Class destructor.
template<typename T>
Hubbard<T>::~Hubbard () {}

template<typename T>
int64 Hubbard<T>::Dim () { return dim_; }

template<typename T>
void Hubbard<T>::SetLink (const int64 i, const int64 j, const int64 z, const T a) {
        int64 k = j*numsite_x_+i;
        hoppings_[k*2+z] = a;
        }

template<typename T>
T Hubbard<T>::Dot (const T* v, const T* w) {
        T res = 0.0;
        for (auto it = basis_xy_.begin (); it != basis_xy_.end (); ++it) {
            int64 l = std::distance (basis_xy_. begin (), it);
            res += v[l]*std::conj (w[l]);
            }
        return res;
        }

template<typename T>
int64 Hubbard<T>::Permutation (std::vector<int64> &seq) {
        const int64 len = seq.size ();
        int64 count = 0;
        for (int64 i = 0; i < (len-1); ++i) {
            for (int64 j = 0; j < (len-1-i); ++j) {
                if (seq[j] > seq[j+1]) {
                    std::swap (seq[j], seq[j+1]);
                    ++count;
                    }
                }
            }
        return count;
        }

template<typename T>
int64 Hubbard<T>::TranslationX (const int64 b) {
        boost::dynamic_bitset<> config (numsite_*2, b);
        boost::dynamic_bitset<> temp (numsite_*2);
        std::vector<int64> order;
        for (int64 spin = 0; spin < 2; ++spin) {
            for (int64 j = 0; j < numsite_y_; ++j) {
                for (int64 i = 0; i < numsite_x_; ++i) {
                    int64 k = Coordinate (i, j, spin);
                    if (config[k]) {
                        int64 kk = Coordinate ((i+1) % numsite_x_, j, spin);
                        order.push_back (kk);
                        temp[kk] = 1;
                        }
                    }
                }
            }
        int64 num = Permutation (order);
        int64 sgn = 1;
        if (num & 1) { sgn = -1; } // faster than "num % 2"
        auto it = std::lower_bound (basis_.begin (), basis_.end (), temp.to_ulong ());
        return sgn*basis_[std::distance (basis_.begin (), it)];
        }

template<typename T>
int64 Hubbard<T>::TranslationY (const int64 b) {
        boost::dynamic_bitset<> config (numsite_*2, b);
        boost::dynamic_bitset<> temp (numsite_*2);
        std::vector<int64> order;
        for (int64 spin = 0; spin < 2; ++spin) {
            for (int64 j = 0; j < numsite_y_; ++j) {
                for (int64 i = 0; i < numsite_x_; ++i) {
                    int64 k = Coordinate (i, j, spin);
                    if (config[k]) {
                        int64 kk = Coordinate (i, (j+1) % numsite_y_, spin);
                        order.push_back (kk);
                        temp[kk] = 1;
                        }
                    }
                }
            }
        int64 num = Permutation (order);
        int64 sgn = 1;
        if (num & 1) { sgn = -1; } 
        auto it = std::lower_bound (basis_.begin (), basis_.end (), temp.to_ulong ());
        return sgn*basis_[std::distance (basis_.begin (), it)];
        }

template<typename T>
void Hubbard<T>::TranslationXYBasis () {
        TranslationTag null_tag (-1, -1, -1, 0.0);
        tag_basis_xy_ = std::vector<TranslationTag> (dim_, null_tag); // tags attached to every basis in the original Hilbert space, convenient for the Hamiltoian construction
        std::vector<bool> marker (dim_, true); // mark the basis if it has been considered
        for (auto it = basis_.begin (); it != basis_.end (); ++it) {
            int64 i = std::distance (basis_.begin (), it);
            if (marker[i]) {
                std::vector<int64> seq_val;
                std::vector<double> seq_sgn;
                int64 ref = *it;
                int64 val = std::abs (ref);
                double sgn_x = 1.0; // trace the fermion sign
                // put the first config into the sequence
                seq_val.push_back (val);
                seq_sgn.push_back (sgn_x);
                marker[i] = false;
                // translation along x-direction
                int64 cfg_x = TranslationX (val);
                int64 val_x = std::abs (cfg_x);
                if (cfg_x < 0) { sgn_x *= -1.0; }
                int64 period_x = 1; // be careful about the defination of period
                while ( val_x != ref) {
                    ++period_x;
                    seq_val.push_back (val_x);
                    seq_sgn.push_back (sgn_x);
                    auto itx = std::lower_bound (basis_.begin (), basis_.end (), val_x);
                    marker[std::distance (basis_.begin (), itx)] = false;
                    cfg_x = TranslationX (val_x);
                    val_x = std::abs (cfg_x);
                    if (cfg_x < 0) { sgn_x *= -1.0; }
                    }
                cfg_x = TranslationX (val_x); // translate a further step to obtain the sign of one x-period
                val_x = std::abs (cfg_x);
                if (cfg_x < 0) { sgn_x *= -1.0; }
                double sgn_period_x = sgn_x;
                // then translate along y-direction
                int64 cfg_y = TranslationY (ref);
                int64 val_y = std::abs (cfg_y);
                double sgn_y = 1.0;
                if (cfg_y < 0) { sgn_y *= -1.0; }
                int64 period_y = 1;
                while (val_y != ref) {
                    ++period_y;
                    seq_val.push_back (val_y);
                    seq_sgn.push_back (sgn_y);
                    auto ity = std::lower_bound (basis_.begin (), basis_.end (), val_y);
                    marker[std::distance (basis_.begin (), ity)] = false;
                    // translate along x-direction for each y-translation
                    val_x = val_y;
                    sgn_x = sgn_y;
                    for (int64 l = 0; l < (period_x-1); ++l) {
                      cfg_x = TranslationX (val_x);
                      val_x = std::abs (cfg_x);
                      if (cfg_x < 0) { sgn_x *= -1.0; }
                      seq_val.push_back (val_x);
                      seq_sgn.push_back (sgn_x);
                      auto itx = std::lower_bound (basis_.begin (), basis_.end (), val_x);
                      marker[std::distance (basis_.begin (), itx)] = false;
                      }
                    cfg_y = TranslationY (val_y);
                    val_y = std::abs (cfg_y);
                    if (cfg_y < 0) { sgn_y *= -1.0; }
                    }
                cfg_y = TranslationY (val_y); // translate a further step to obtain the sign of one y-period
                val_y = std::abs (cfg_y);
                if (cfg_y < 0) { sgn_y *= -1.0; }
                double sgn_period_y = sgn_y;
                // std::cout << std::distance (basis_.begin (), it) << " " << dim_ << std::endl;
                // for (auto its = seq_val.begin (); its != seq_val.end (); ++ its) {
                    // boost::dynamic_bitset<> config (numsite_*2, *its);
                    // std::cout << config << " " << seq_sgn[std::distance (seq_val.begin (), its)];
                    // std::cout << std::endl;
                    // }
                int64 num_period_x = int64 (numsite_x_ / period_x);
                int64 num_period_y = int64 (numsite_y_ / period_y);
                std::complex<double> gam_x = 0.0;
                std::complex<double> gam_y = 0.0;
                for (int64 l = 0; l < num_period_x; ++l) { gam_x += std::exp (-1.0*IMAG_I*kx_*double (period_x*l)) * std::pow (sgn_period_x, l); }
                for (int64 l = 0; l < num_period_y; ++l) { gam_y += std::exp (-1.0*IMAG_I*ky_*double (period_y*l)) * std::pow (sgn_period_y, l); }
                double gam_square_x = std::real (gam_x * std::conj (gam_x));
                double gam_square_y = std::real (gam_y * std::conj (gam_y));
                double norm = period_x*period_y*gam_square_x*gam_square_y;
                if (norm > 1e-15) {
                    basis_xy_.push_back (std::make_pair (seq_val.front (), norm)); // seq_val.front () is already the minimal number in the sequence
                    int64 idx = std::distance (basis_xy_.begin (), basis_xy_.end ()-1); // denote the current position of this basis in the reduced Hilbert space
                    // attach a tag to each vector in the sequence
                    for (auto it_seq = seq_val.begin (); it_seq != seq_val.end (); ++it_seq) {
                        int64 pos = std::distance (seq_val.begin (), it_seq);
                        int64 px = pos % period_x;
                        int64 py = pos / period_x;
                        TranslationTag tag (idx, px, py, seq_sgn[pos]);
                        auto it_tag = std::lower_bound (basis_.begin (), basis_.end (), *it_seq);
                        tag_basis_xy_[std::distance (basis_.begin (), it_tag)] = tag;
                        }
                    }
                }
            }
            dim_xy_ = basis_xy_.size ();
        }

template<typename T>
int64 Hubbard<T>::DimXY () { return dim_xy_; }

// construct the sparse matrix
template<typename T>
void Hubbard<T>::CSCHamiltonianXY () {
        int64 count = 0;
        for (auto it = basis_xy_.begin (); it != basis_xy_.end (); ++it) {
            int64 l = std::distance (basis_xy_.begin (), it);
            boost::dynamic_bitset<> config (numsite_*2, it->first);
            std::map<int64, T, std::less<int64>> map_col; // sorted map in the asending order
            for (int64 current_site = 0; current_site < numsite_; ++current_site) { // current site
                // Hubbard U
                if (config[current_site] & config[numsite_+current_site]) {
                    auto it_u = map_col.find (l);
                    if (it_u == map_col.end ()) { map_col.insert (std::make_pair (l, u_)); } // if not found
                    else { it_u->second += u_; }
                    }
                // hoppings
                for (int64 spin = 0; spin < 2; ++spin) {
                    // move forward
                    for (int64 move = 0; move < 2; ++move) {
                        int64 forward_site = sites_forward_[current_site][move]; // forward site
                        int64 current_bit = spin*numsite_+current_site;
                        int64 forward_bit = spin*numsite_+forward_site;
                        if ((config[current_bit] ^ config[forward_bit])) {
                            T phase = 0.0;
                            if (1 == config[current_bit]) { phase = hoppings_[current_site*2+move]; }
                            else { phase = std::conj (hoppings_[current_site*2+move]); }
                            double fermion_sgn = 1.0;
                            int64 low = current_bit;
                            int64 high = forward_bit;
                            if (forward_site < current_site) {
                                low = forward_bit;
                                high = current_bit;
                                }
                            for (int64 m = low+1; m < high; ++m) { if (config[m]) { fermion_sgn *= -1.0; } }
                            boost::dynamic_bitset<> temp (config);
                            temp.flip (current_bit);
                            temp.flip (forward_bit);
                            auto it_flipped = std::lower_bound (basis_.begin (), basis_.end (), int64 (temp.to_ulong ()));
                            auto tag_flipped = tag_basis_xy_[std::distance (basis_.begin (), it_flipped)];
                            int64 k = tag_flipped.Index (); // get the basis position in the reduced Hilbert space
                            int64 px = tag_flipped.PositionX ();
                            int64 py = tag_flipped.PositionY ();
                            double sgn = tag_flipped.Sign ();
                            std::complex<double> ele = -1.0*phase*fermion_sgn*sgn;
                            ele *= std::exp (-1.0*IMAG_I*(kx_*px+ky_*py)) * std::sqrt (basis_xy_[k].second / basis_xy_[l].second);
                            auto it_col = map_col.find (k);
                            if (it_col == map_col.end ()) { map_col.insert (std::make_pair (k, ele)); }
                            else { it_col->second += ele; }
                            }
                        }
                    }
                }
            for (auto it_col = map_col.begin (); it_col != map_col.end (); ++it_col) {
                index_col_xy_.push_back (l);
                index_row_xy_.push_back (it_col->first);
                ham_matrix_xy_.push_back (it_col->second);
                ++count;
                }
            }
        num_nzero_xy_ = count;
        std::cout << num_nzero_xy_ << std::endl;
        }

template<typename T>
void Hubbard<T>::MultiVecXY (T* v, T* w) {
        std::fill_n (w, dim_xy_, 0.0);
        for (int64 l = 0; l < num_nzero_xy_; ++l) { w[index_row_xy_[l]] += ham_matrix_xy_[l]*v[index_col_xy_[l]]; }
        }

/*
template<typename T>
void Hubbard<T>::HoppingXIJ (int64 y, int64 spin, T* v, T* w) {
        std::fill_n (w, dim_X, 0.0);
        for (auto it = basis_X.begin (); it != basis_X.end (); ++it) {
            int64 l = std::distance (basis_X.begin (), it);
            int64 a = it->first;
            boost::dynamic_bitset<> config (numsite_*2, a);
            for (int64 s = 0; s < numsite_x_; ++s) {
                int64 i = spin*numsite_+y*numsite_x_+s;
                int64 j = spin*numsite_+y*numsite_x_+((s+1) % numsite_x_);
                if (0 == config[i] && 1 == config[j]) {
                    double fermionSign = 1.0;
                    for (int64 m = std::min (i, j)+1; m < std::max (i, j); ++m) { if (config[m]) { fermionSign = -1.0*fermionSign; } }
                    boost::dynamic_bitset<> temp (config);
                    temp.flip (i);
                    temp.flip (j);
                    auto itt = std::lower_bound (basis_.begin (), basis_.end (), int64 (temp.to_ulong ()));
                    auto rep = basis_XIndex[std::distance (basis_.begin (), itt)];
                    int64 k = rep.Index ();
                    int64 pos = rep.Position ();
                    double sign = rep.Sign ();
                    w[k] += fermionSign*sign*std::exp (-1.0*IMAG_I*(kx_*pos))*std::sqrt (basis_X[k].second / basis_X[l].second)*v[l];
                    }
                }
            }
        }

template<typename T>
void Hubbard<T>::HoppingXJI (int64 y, int64 spin, T* v, T* w) {
        std::fill_n (w, dim_X, 0.0);
        for (auto it = basis_X.begin (); it != basis_X.end (); ++it) {
            int64 l = std::distance (basis_X.begin (), it);
            int64 a = it->first;
            boost::dynamic_bitset<> config (numsite_*2, a);
            for (int64 s = 0; s < numsite_x_; ++s) {
                int64 i = spin*numsite_+y*numsite_x_+s;
                int64 j = spin*numsite_+y*numsite_x_+((s+1) % numsite_x_);
                if (0 == config[j] && 1 == config[i]) {
                    double fermionSign = 1.0;
                    for (int64 m = std::min (i, j)+1; m < std::max (i, j); ++m) { if (config[m]) { fermionSign = -1.0*fermionSign; } }
                    boost::dynamic_bitset<> temp (config);
                    temp.flip (i);
                    temp.flip (j);
                    auto itt = std::lower_bound (basis_.begin (), basis_.end (), int64 (temp.to_ulong ()));
                    auto rep = basis_XIndex[std::distance (basis_.begin (), itt)];
                    int64 k = rep.Index ();
                    int64 pos = rep.Position ();
                    double sign = rep.Sign ();
                    w[k] += fermionSign*sign*std::exp (-1.0*IMAG_I*(kx_*pos))*std::sqrt (basis_X[k].second / basis_X[l].second)*v[l];
                    }
                }
            }
        }
*/
