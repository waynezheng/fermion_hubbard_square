class TransRepX {
        private:
        int index_;
        int pos_;
        double sign_;

        public:
        TransRepX (int i, int p, double s);
        ~TransRepX ();

        int Index () { return index_; }
        int Position () { return pos_; }
        double Sign () { return sign_; }
        };

TransRepX::TransRepX (int i, int p, double s) {
        index_ = i;
        pos_ = p;
        sign_ = s;
        }

TransRepX::~TransRepX () {}


class TranslationTag {
        private:
        long long int index_;
        int posX_;
        int posY_;
        double sign_;

        public:
        TranslationTag (const long long int i, const int px, const int py, const double s);
        ~TranslationTag ();

        long long int Index () { return index_; }
        int PositionX () { return posX_; }
        int PositionY () { return posY_; }
        double Sign () { return sign_; }
        };

TranslationTag::TranslationTag (const long long int i, const int px, const int py, const double s) {
        index_ = i;
        posX_ = px;
        posY_ = py;
        sign_ = s;
        }

TranslationTag::~TranslationTag () {}
