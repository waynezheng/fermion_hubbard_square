#include <iostream>
#include <fstream>
#include <random>
#include<iomanip>
#include <memory.h>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <cmath>
#include <stdlib.h>
#include <ctime>
#include <algorithm>
#include <initializer_list>
#include <vector>
#include <bitset>
#include <complex>
#include <map>
#include <limits>

// Arpackpp
// #include <arlnsmat.h> // ARluNonSymMatrix
// #include <arlscomp.h> // ARluCompStdEig

#include <arssym.h>
#include <arcomp.h>
#include <arscomp.h>

// boost
#include <boost/dynamic_bitset.hpp>

static const double PI = std::acos (-1.0);
static const std::complex<double> IMAG_I = std::complex<double> (0.0, 1.0);

int main ();
