#include "head.hpp"
#include "class_hubbard.hpp"

int main() {
        const int LINE = 0;

        std::ifstream cFile;
        std::string line;
        cFile.open("config.csv");
        for (int i = 0; i < (LINE+1); ++i) { std::getline (cFile, line); }
        std::getline (cFile, line);
        std::cout << line << std::endl;

        std::stringstream ss (line);
        std::string word;
        std::getline (ss, word, ',');
        const int kNumUp = std::stoi (word);
        std::getline (ss, word, ',');
        const int kNumDown = std::stoi (word);
        std::getline (ss, word, ',');
        const int kNumSiteX = std::stoi (word);
        std::getline (ss, word, ',');
        const int kNumSiteY = std::stoi (word);
        std::getline (ss, word, ',');
        const std::string kBoundaryX = word;
        std::getline (ss, word, ',');
        const std::string kBoundaryY = word;

        const int kNumSam = 1;
        const int kNumEval = 10;

        std::cout << kNumUp << " " << kNumDown << " " << kNumSiteX << " " << kNumSiteY << " " << kBoundaryX << " " << kBoundaryY << std::endl;

        time_t start, end;
        start = time (NULL);

        double U = 12.0;
        double step = U;

        std::ofstream file_log ("log", std::ios_base::app);
        std::ofstream file_eigvals ("eigenvalues.dat", std::ios_base::app | std::ios_base::binary);
        std::ofstream file_eigvecs ("eigenvectors.dat", std::ios_base::app | std::ios_base::binary);
        std::ofstream file_data ("results.dat", std::ios_base::app);

        file_data << "u," << "gsE," <<  "transv," << "szCor" << std::endl;

        for (int s = 0; s < kNumSam; ++s) {
            Hubbard<std::complex<double>> Hub (U, kNumUp, kNumDown, kNumSiteX, kNumSiteY, kBoundaryX, kBoundaryY);
            time_t construction_start = time (NULL);
            Hub.CSCHamMatrix ();
            time_t construction_end = time (NULL);
            std::cout << "Construction time: " << (construction_end - construction_start) << " s" << std::endl;
            int dim = Hub.Dim ();
            std::cout << dim << std::endl;
            ARCompStdEig<double, Hubbard<std::complex<double>>> EigProb;
            EigProb.DefineParameters (dim, kNumEval, &Hub, &Hubbard<std::complex<double>>::MultiVec, "SR", 50);
            auto eig_val = new std::complex<double> [kNumEval];
            auto eig_vec = new std::complex<double> [kNumEval*dim];
            int nconv = EigProb.EigenValVectors (eig_vec, eig_val);
            // to sort the eigenvalues and keep track of the index
            std::vector<std::pair<double, int>> temp;
            for (int j = 0; j < nconv; ++j) { temp.push_back (std::pair<double, int> (std::real (eig_val[j]), j)); }
            std::sort (temp.begin (), temp.end ());
            for (int j = 0; j < nconv; ++j) {
                double val = temp[j].first;
                int index = temp[j].second;
                file_eigvals.write ((char*)(&val), sizeof (double));
                for (int k = 0; k < dim; ++k) {
                    int d = index*dim+k;
                    double rr = std::real (eig_vec[d]);
                    double ii = std::imag (eig_vec[d]);
                    file_eigvecs.write ((char*)(&rr), sizeof (double));
                    file_eigvecs.write ((char*)(&ii), sizeof (double));
                    }
                }

            delete [] eig_val;
            delete [] eig_vec;

            std::cout << std::setprecision (4) << U << ": " << std::setprecision (15) << eig_val[temp[0].second] << " " << eig_val[temp[1].second] << std::endl;
            U += step;
            }
        end = time (NULL);
        file_log << "Time: " << (end-start) / 60.0 << " min" << std::endl;
        std::cout << "Total time: " << (end-start) << " s" << std::endl;
        file_eigvals.close ();
        file_eigvecs.close ();
        file_log.close ();
        return 1;
    }
