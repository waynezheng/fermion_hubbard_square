#include "head.hpp"
#include "class_hubbard_translation_xy.hpp"

int main() {
        const int LINE = 0;

        std::ifstream cFile;
        std::string line;
        cFile.open("config.csv");
        for (int i = 0; i < (LINE+1); ++i) { std::getline (cFile, line); }
        std::getline (cFile, line);
        std::cout << line << std::endl;

        std::stringstream ss (line);
        std::string word;
        std::getline (ss, word, ',');
        const int numEleUp = std::stoi (word);
        std::getline (ss, word, ',');
        const int numEleDown = std::stoi (word);
        std::getline (ss, word, ',');
        const int numSiteX = std::stoi (word);
        std::getline (ss, word, ',');
        const int numSiteY = std::stoi (word);
        std::getline (ss, word, ',');
        const std::string bouConX = word;
        std::getline (ss, word, ',');
        const std::string bouConY = word;

        const int numSam = 1;
        const int numEval = 10;

        std::cout << numEleUp << " " << numEleDown << " " << numSiteX << " " << numSiteY << " " << bouConX << " " << bouConY << std::endl;

        time_t start, end;
        start = time (NULL);

        double U = 12.0;
        // double step = U;

        std::ofstream file_log ("log", std::ios_base::app);
        std::ofstream file_eigvals ("eigenvalues.dat", std::ios_base::app | std::ios_base::binary);
        std::ofstream file_eigvecs ("eigenvectors.dat", std::ios_base::app | std::ios_base::binary);
        std::ofstream file_data ("results.dat", std::ios_base::app);

        for (int s = 0; s < numSam; ++s) {
            Hubbard<std::complex<double>> Hub (U, numEleUp, numEleDown, numSiteX, numSiteY, bouConX, bouConY, 0, 0);
            int dim = Hub.Dim ();
            std::cout << "HS dim " << dim <<std::endl;
            Hub.TranslationXYBasis ();
            int dimXY = Hub.DimXY ();
            std::cout << "Reduced HS dim " << dimXY <<std::endl;
            time_t conStart = time (NULL);
            Hub.CSCHamiltonianXY ();
            time_t conEnd = time (NULL);
            std::cout << "Hamiltonian matrix construction time: " << (conEnd-conStart) << " s" << std::endl;
            ARCompStdEig<double, Hubbard<std::complex<double>>> EigProb;
            EigProb.DefineParameters (dim, numEval, &Hub, &Hubbard<std::complex<double>>::MultiVecXY, "SR", 100);
            auto eigVal = new std::complex<double> [numEval];
            auto eigVec = new std::complex<double> [numEval*dim];
            int nconv = EigProb.EigenValVectors (eigVec, eigVal);
            // To sort the eigenvalues and keep track of the index
            std::vector<std::pair<double, int>> temp;
            for (int j = 0; j < nconv; ++j) { temp.push_back (std::pair<double, int> (std::real(eigVal[j]), j)); }
            std::sort (temp.begin (), temp.end ());
            for (int j = 0; j < nconv; ++j) {
                double val = temp[j].first;
                int index = temp[j].second;
                file_eigvals.write ((char*)(&val), sizeof (double));
                for (int k = 0; k < dim; ++k) {
                    int d = index*dim+k;
                    double rr = std::real (eigVec[d]);
                    double ii = std::imag (eigVec[d]);
                    file_eigvecs.write ((char*)(&rr), sizeof (double));
                    file_eigvecs.write ((char*)(&ii), sizeof (double));
                    }
                }
            /*
            auto gs = new std::complex<double> [dim];
            auto s0 = new std::complex<double> [dim];
            auto s1 = new std::complex<double> [dim];
            for (int k = 0; k < dim; ++k) { gs[k] = eigVec[(temp[0].second)*dim+k]; }

            int spin = 1;
            Hub.HoppingXIJ (0, spin, gs, s0);
            Hub.HoppingXJI (0, spin, gs, s1);
            auto c0 = Hub.Dot (gs, s0);
            auto c1 = Hub.Dot (gs, s1);
            auto c = c0-c1;

            std::cout << c << std::endl;
            delete [] gs;
            delete [] s0;
            delete [] s1;
            */
            delete [] eigVal;
            delete [] eigVec;
            // std::cout << std::setprecision (4) << U << ": " << std::setprecision (15) << eigVal[temp[0].second] << " " << eigVal[temp[1].second] << ", " << 0.5*c << std::endl;
            std::cout << std::setprecision (4) << U << ": " << std::setprecision (15) << eigVal[temp[0].second] << " " << eigVal[temp[1].second] << std::endl;
            // file_data << std::setprecision (4) << U << "," << std::setprecision (8) << temp[0].first << std::endl;
            // U += step;
            }
        end = time (NULL);
        file_log << "Time: " << (end-start)/60.0 << " min" << std::endl;
        std::cout << "Total time: " << (end-start) << " s" << std::endl;
        file_eigvals.close ();
        file_eigvecs.close ();
        file_log.close ();
        return 1;
    }
