 #!/bin/bash

X=06
Y=02
UE=06
DE=05

SPEC=10

START=0.2
STEP=0.2
NUMSAM=2

 mv eigenvalues.dat eigenvalues${SPEC}_lattice${X}${Y}OO_filling${UE}${DE}_U${START}_step${STEP}_num${NUMSAM}.dat 
 mv eigenvectors.dat eigenvectors${SPEC}_lattice${X}${Y}OO_filling${UE}${DE}_U${START}_step${STEP}_num${NUMSAM}.dat 
 mv results.dat results_lattice${X}${Y}OO_filling${UE}${DE}_U${START}_step${STEP}_num${NUMSAM}.csv
 mv hilbert_space.dat hilbert_space_lattice${X}${Y}OO_filling${UE}${DE}.txt

 mv *.dat *.txt ../data

