\documentclass[aps, prb, reprint]{revtex4-2} %{revtex4-1}%
% \documentclass[eprint]{article}
\usepackage{amssymb}  % include amsfonts package
\usepackage{amsfonts}  % American Math Society fonts, define \mathbb, \mathfrak 
\usepackage{amsmath}  % multi-lines formulars, \cfrac
\usepackage{amsthm}  % provide a PROOF enviroment
\renewcommand\qedsymbol{$\blacksquare$}
\usepackage{bm}  % bold math
\usepackage{bbm}  % hollow math
\usepackage{romannum} % Roman number
\usepackage[]{hyperref}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{bbding}

\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}{Corollary}[theorem]

% Tikz and relatedTikz and related.
\usepackage{tikz, datatool, ifthen, csvsimple}
\usetikzlibrary{arrows, shapes, math, decorations, decorations.markings}
\usetikzlibrary{calc}
\usepackage{pgfplots}
\pgfplotsset{compat=newest}
% \usepgfplotslibrary{external}
% \tikzexternalize
\pgfplotsset{
    colormap={mycm}{rgb255=(225, 225, 225) rgb255=(225, 225, 225)},
    colormap/mycm/.style={
        colormap name=mycm,
    },
}
\usepackage{pgfmath}
\input pgfmath.tex

\begin{document}
\title{Note on Hubbard model}
\author{Wayne Zheng}
\date{\today}
\maketitle

\tableofcontents

\section{Introduction}

\section{Exact results}
\subsection{Lieb-Wu solution}
The exact solution for 1D half-filed Hubbard model is
\begin{equation}
E_{0}(N/2, N/2)=-4N\int_{0}^{\infty}\frac{J_{0}(\omega)J_{1}(\omega)}{\omega\left(1+e^{\omega U/2}\right)}d\omega
\end{equation}
where $J_{0, 1}$ are the Bessel function of first and second order. Note that it is only valid in the thermodynamic limit as $L \rightarrow \infty$.

\subsection{Lieb's theorem on Hubbard model}
For the hall filled Hubbard model, in the limit $U \rightarrow \infty$ Hubbard model is reduced to Heisenberg model. Even for finite $U$, Lieb\cite{PhysRevLett.62.1201} has proven powerful theorems to state that the ground state property is quite similar to the Heisenberg model. \emph{There is no ground state degeneracy for repulsive half-filled Hubbard model}. That is, any finite small $U$ can dramatically change the the property of free fermions because we can always expect at most four-fold ground state degeneracy for free fermions on a bipartite lattice. Lieb's theorems tell us that any finite $U$ will destroy thus ground state degeneracy.

It is quite natural to ask how about the scenario away hall-filling. Nothing like Lieb's theorems tell us the story.

\section{Ferromagnetism}
\subsection{Spin operators and symmetries in Hubbard model}
Local spin operator can be defined as
\begin{equation}
\mathbf{S}_{i}=\frac{1}{2}F_{i}^{\dagger}\mathbf{\sigma}F_{i}, \quad F_{i}=(c_{i\uparrow}, c_{i\downarrow})^{T}.
\end{equation}
Total $S^{z}=\frac{1}{2}\sum_{i}S_{i}^{z}=\frac{1}{2}\sum_{i}(n_{i\uparrow}-n_{i\downarrow})$ is conserved in Hubbard model as $[H, S^{z}]=0$. Total $\mathbf{S}^{2}=(S^{z})^{2}+\frac{1}{2}(S^{+}S^{-}+S^{-}S^{+})=S_{\text{tot}}(S_{\text{tot}+1})$, where $S^{+}=S^{x}+\text{i}S^{y}=(S^{-})^{\dagger}=\sum_{i}c_{i\uparrow}^{\dagger}c_{i\downarrow}$. $[S^{x}, S^{y}]=\text{i}S^{z}$ forms a SU(2) algebra. The fact that Hubbard model Hamiltonian commuting with these operators implies it has a SU(2) symmetry. We would like to measure the quantity $(S^{T})^{2}=\frac{1}{2}(S^{+}S^{-}+S^{-}S^{+})$. Specifically we carry on the operator on a wavefunction as
\begin{equation}
\begin{aligned}
&{S^{-}S^{+}}|\psi\rangle \\
&=\sum_{i,j}c_{i\downarrow}^{\dagger}c_{i\uparrow}c_{j\uparrow}^{\dagger}c_{j\downarrow}|\psi\rangle \\
&=\sum_{\alpha}c_{\alpha}\left(\sum_{i,j}c_{i\downarrow}^{\dagger}c_{i\uparrow}c_{j\uparrow}^{\dagger}c_{j\downarrow}\right)|\alpha\rangle \\
&=\sum_{\alpha}c_{\alpha}\left(\sum_{i,j}c_{i\downarrow}^{\dagger}c_{j\downarrow}c_{i\uparrow}c_{j\uparrow}^{\dagger}\right)|\alpha\rangle \\
&=\sum_{\alpha}c_{\alpha}\left[\sum_{i}(n_{i\downarrow}-n_{i\downarrow}n_{i\uparrow})-\sum_{i\neq j}c_{i\downarrow}^{\dagger}c_{j\downarrow}c_{j\uparrow}^{\dagger}c_{i\uparrow}\right]|\alpha\rangle.
\end{aligned}
\end{equation}

Under particle-hole transformation $c_{i\sigma}^{\dagger}\rightarrow c_{i\sigma}$, the Hubbard Hamiltonian transforms to
\begin{equation}
    H^{\prime}=t\sum_{\langle{ij}\rangle}(c_{i\sigma}^{\dagger}c_{j\sigma}+h.c.)+U\sum_{i}n_{i\uparrow}n_{i\downarrow}+U(|\Lambda|-N_{\text{e}}).
    \label{eq:}
\end{equation}
For a bipartite lattice, it can be further followed with a sign change on sublattice $B$ as $c_{i\sigma}^{\dagger}\rightarrow -c_{i\sigma}^{\dagger}, \forall~i\in B$, which will change the sign of kinetic energy. That is, at half-filling $N_{\text{e}}=|\Lambda|$, Eq. (\ref{eq:ham_hubbard}) is invariant under particle-hole transformation.

\section{Quantum entanglement}
It is quite natural to consider the entanglement of the two branches of fermions in Lieb's representation\cite{PhysRevLett.62.1201}:
\begin{equation}
    |\psi\rangle=\sum_{\alpha\beta}W_{\alpha\beta}|\alpha\rangle_{\uparrow}\otimes|\beta\rangle_{\downarrow}.
\end{equation}
The reduced density matrix is $\rho\equiv WW^{\dagger}$ and the corresponding Von Neumann entanglement entropy is $S=-\text{tr}(\rho\ln\rho)$, which is called \emph{mutual spin entanglement entropy} (MSEE), in order to characterize the fermion interaction feratures from the perspective of quantum entanglement.
Alternatively, we can still compute the conventional spatial biparte entanglement entropy (SBEE) as a comparsion.

\section{Numerical results}
\begin{figure}[!h]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/energy_lattice42.pdf}
    \caption{Energy of Hubbard model on a $4\times 2$ lattice with OBC. (a) and (b) refer to half-filled. (c) and (d) refer to $N_{\text{e}}=N_{\text{s}}-1$.}
    \label{fig:}
\end{figure}

\begin{figure}[!h]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/profiles.pdf}
    \caption{The profile of physical quantities in the ground state of Hubbard on a $4\times 2$ lattice with OBC (On one edge). (a) and (c) refer to $\langle S_{i}^{z}\rangle$. (b) and (d) refer to $\langle n_{i}\rangle$. (e) and (f) refer to spin correlator $\langle S_{0}^{z}S_{j}^{z}\rangle$. (a), (b) and (c) refer to half-filled case. (d), (e) and (f) refer to one hole away half-filling.}
    \label{fig:}
\end{figure}

\begin{table}[!h]
    \centering
    \begin{tabular}{p{0.15\textwidth}p{0.15\textwidth}p{0.15\textwidth}}
        \hline\hline
        lattice & $U{\text{c}}/t$ & $S_{\text{tot}}$ \\
        \hline
        $2\times2$ & $18.0$ & $1/2, 3/2$ \\
        $4\times2$ & $45.0, 84.0$ & $1/2, 3/2, 7/2$ \\
        $3\times3$ & $69.0$ & $0, 4$ \\
        \hline\hline
    \end{tabular}
    \caption{Nagaoka transition on finte lattices with one hole doped $N_{\text{e}}=N_{\text{s}}-1$ with OBC.}
    \label{tab:}
\end{table}

\begin{figure}[!h]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/gs_EE.pdf}
    \caption{Entanglement entropy in the ground state of Hubbard model on a $4\times 2$ lattice with OBC. (a) refers to the half-filled case. (b) refers to the one hole doped case.}
    \label{fig:}
\end{figure}

\newpage
\appendix

\section{Reivew on Lieb's theorems on Hubbard model}
Lieb first uses reflection positivity to prove the uniqueness of a strongly correlated fermion system\cite{PhysRevLett.62.1201} and its optimal flux phase\cite{PhysRevLett.73.2158}. There are also some recent progress\cite{PhysRevB.92.161105}\cite{PhysRevLett.116.250601}.

Consider the Hubbard model on a bipartite square lattice $\Lambda$ consisting of $|\Lambda|$ sites,
\begin{equation}
    H=\sum_{\langle{ij}\rangle}t_{ij}c_{i\sigma}^{\dagger}c_{j\sigma}+U\sum_{j}n_{j\uparrow}n_{j\downarrow}.
    \label{eq:ham_hubbard_gen}
\end{equation}
For large replusive $U$, we have known that Eq. (\ref{eq:ham_hubbard}) is equivalent to Heisenberg model $H=\left( 2/U \right)\sum_{\langle{ij}\rangle}t_{ij}\left( \mathbf{S}_{i}\cdot\mathbf{S}_{j}-1/4 \right)$, of which ground state is unique and equipped with Marshall sign structure\cite{rspa.1955.0200}\cite{auerbach1998}. Lieb made this progress to prove that for not only large but any finite $U$, the ground state of Eq. (\ref{eq:ham_hubbard}) is unique.

\subsection{Attractive $U$}
Suppose $U<0$ and there are $N (N \leqslant 2|\Lambda|)$ electrons. We choose to work in the subpace $S^{z}=0$ which means that the number of each species of spinless fermion is $n=\frac{1}{2}N$. Generally we can compose a set of real orthogonal basis $\left\{ |\alpha\rangle \right\}$ of such one piece spinless fermions and the dimension is $m=C_{|\Lambda|}^{n}$. The ground state can be written in this basis as
\begin{equation}
    |\psi\rangle=\sum_{\alpha, \beta}W_{\alpha\beta}|\alpha\rangle_{\uparrow}\otimes|\beta\rangle_{\downarrow},
    \label{}
\end{equation}
where $W_{\alpha\beta}$ is the coefficients and also can be viewed as a $m \times m$ matrix. The norm reads
\begin{equation}
    \langle\psi|\psi\rangle=\sum_{\alpha, \beta}W_{\alpha\beta}^{*}W_{\alpha\beta}=\sum_{\alpha, \beta}(W^{\dagger})_{\beta\alpha}W_{\alpha\beta}=\text{tr}\left( W^{2} \right).
    \label{}
\end{equation}
and we assume this to be unity. We can further assume $W^{\dagger}=W$. The kinetic energy for one piece
\begin{equation}
    \begin{aligned}
        T(W)
        &=\langle\psi|\sum_{\langle{ij}\rangle, \sigma}t_{ij}c_{i\sigma}^{\dagger}c_{j\sigma}|\psi\rangle \\
        &=2\sum_{\alpha, \beta, \alpha{'}, \beta{'}}\langle\alpha{'}|\sum_{\langle{ij}\rangle}t_{ij}c_{i}^{\dagger}c_{j}|\alpha\rangle\delta_{\beta{'}\beta}W_{\alpha{'}\beta{'}}^{*}W_{\alpha\beta} \\
        &=2\text{tr}\left( W^{\dagger}KW \right)=2\text{tr}\left( KW^{2} \right)
    \end{aligned}
    \label{}
\end{equation}
with $K_{\alpha{'}\alpha}=\langle\alpha{'}|\sum_{\langle{ij}\rangle}t_{ij}c_{i}^{\dagger}c_{j}|\alpha\rangle$. On site energy is
\begin{equation}
    \begin{aligned}
        V(W)
        &=U\langle\psi|\sum_{j}n_{j\uparrow}n_{j\downarrow}|\psi\rangle \\
        &=U\sum_{j}\sum_{\alpha, \beta, \alpha{'}, \beta{'}}\langle\alpha{'}|n_{j\uparrow}|\alpha\rangle\langle\beta^{'}|n_{j\downarrow}|\beta\rangle W_{\alpha{'}\beta{'}}^{*}W_{\alpha\beta} \\
        &=U\sum_{j}\sum_{\alpha, \beta, \alpha{'}, \beta{'}}W_{\beta{'}\alpha{'}}^{\dagger}\langle\alpha{'}|n_{j\uparrow}|\alpha\rangle W_{\alpha\beta}\langle\beta|n_{j\downarrow}|\beta{'}\rangle \\
        &=U\sum_{j}\text{tr}\left( W^{\dagger}L_{j}WL_{j}^{\dagger} \right)=U\sum_{j}\text{tr}\left( WL_{j}WL_{j} \right),
    \end{aligned}
    \label{}
\end{equation}
where $\left( L_{j} \right)_{\alpha\alpha^{\prime}}=\langle\alpha|n_{j}|\alpha^{\prime}\rangle$ where we have droped the spin index since we work in a symmetric subspace $S^{z}=0$. The total energy is $E=\langle\psi|H|\psi\rangle=2\text{tr}\left( KW^{2} \right)+U\sum_{j}\left( WL_{j}WL_{j} \right)$. Since $W$ is Hermitian $W=UDU^{\dagger}$, we consider a related positive semidefinite matrix $|W|\equiv U|D|U^{\dagger}$. Obviously $W^{2}=|W|^{2}$ and $T(W)=T(|W|)$. For the interaction part, we compute it in the diagonal basis
\begin{equation}
    \begin{aligned}
        V(W)
        &=\text{tr}\left( WL_{j}WL_{j} \right)=\text{tr}\left( UDU^{\dagger}L_{j}UDU^{\dagger}L_{j} \right) \\
        &=\text{tr}\left[ D\left( U^{\dagger}L_{j}U \right)D\left( U^{\dagger}L_{j}U \right) \right] \\
        &=\sum_{\alpha, \beta}d_{\alpha}d_{\beta}(\widetilde{L}_{j})_{\alpha\beta}(\widetilde{L}_{j})_{\beta\alpha}=\sum_{\alpha, \beta}d_{\alpha}d_{\beta}|(\widetilde{L}_{j})_{\alpha\beta}|^{2} \\
        &\leqslant\sum_{\alpha\beta}|d_{\alpha}||d_{\beta}||(\widetilde{L}_{j})_{\alpha\beta}|^{2}=\text{tr}(|W|L_{j}|W|L_{j}).
    \end{aligned}
    \label{eq:postivity_interaction}
\end{equation}
Since $U<0$, we have $E(|W|) \leqslant E(W)$. Therefore, there exists one ground state $W=|W|$. Actually every ground states should be $W=\pm|W|$. This is the so called \emph{spin-space reflection positity}. In the diagonal basis, the coefficients of the ground state wave function are all non-negative, which correponds to its \emph{sign structrue}. But this basis given by the unitary rotation $U$, is not practically accessible.

Let us suppose there are at least two normalized, Hermitian as well as linear independent ground states $W_{1}$ and $W_{2}$. Then for any real constant $d$, $W_{d}=W_{1}+dW_{2}$ should also be a ground state. However, there exists a $d$ to make $W_{d}$ neither positive nor negative semidefinite, which is contradictory with above.

\subsection{Replusive $U$}

\section{Numerical ED related}
\subsection{Basic setup}
We choose to define a two dimensional fermionic configuration in a \emph{snake form} with a pre-defined specific order for $N$ spinless fermions
\begin{equation}
    |\alpha\rangle\equiv c_{0}^{\dagger}c_{1}^{\dagger}\cdots{c}_{N-1}^{\dagger}|0\rangle.
    \label{eq:fermion_order_def}
\end{equation}

\subsection{Sparse matrix manipulation}
A sparse matrix can be recorded with a one dimensional array \texttt{A[]} to merely store its nonzero matrix elements and with two other integer arraies \texttt{irow[]} and \texttt{icol[]} to trace their row and column indces. These matrix elements can be stored in a \emph{compressed sparse column} (CSC) format, which means that these nonzero numbers are stored column by column and elements in the same column are stored in an increasing order of rows. The number of none zero matrix elements of a sparse matrix defined by a generic local quantum many-body Hamiltonian is roughly the product of the Hilbert space dimension and lattice size. The matrix multiplication is done as \texttt{for (int i = 0; i < nnz; ++i) \{ w[irow[i]] += A[i]*v[icol[i]]; \}}.

\texttt{std::map<int, T, std::less<int>>} is ideal for recording the row index within each column as it is a sorted container according to the unique key. Every inserted element strictly follows the order.

\subsection{Implement translational symmetry on fermions}
As we have defined the fermionic order as Eq.~\ref{eq:fermion_order_def}, to rearrange the order is equivalent to the corresponding \emph{permutation} such as
\begin{equation}
    \begin{pmatrix}
        0 & 1 & \cdots & N-1 \\
        g_{0} & g_{1} & \cdots & g_{N-1}
    \end{pmatrix}.
    \label{eq:}
\end{equation}
Note that the fermion sign arised in this process is dertermined by nothing but the \emph{parity of adjacent transpositions}.
Practically, the number of adjacent transpositions can be naturally obtained by the bubble sort, of which complexity is $O(n^{2})$.
According to the \emph{parity theorem of permutation}, the parity is unchanged even if the decoupled transpositions are different.
That is, there should be some algorithms better than $O(n^{2})$.

\subsubsection{One direction}
For simplicity, let us take a one dimensional ring with $N$ sites. 
Translational operation is defined as $\mathcal{T}c_{i\sigma}^{\dagger}\mathcal{T}^{-1}=c_{i+1, \sigma}^{\dagger}$.
$\mathcal{T}|\alpha\rangle=\zeta|\alpha^{\prime}\rangle$, where $\zeta=\pm 1$ is the fermion sign due to the translation.
With a reference state $|\alpha\rangle$, the corresponding momentum state can be constructed as~\cite{Sandvik1.3518900}
\begin{equation}
    |\alpha(k)\rangle = \frac{1}{\sqrt{\mathcal{N}_{\alpha}}}\sum_{i=0}^{L-1}e^{-\text{i}ki}\mathcal{T}^{i}|\alpha\rangle.
    \label{eq:moment_state}
\end{equation}
We can verify that $\mathcal{T}|\alpha(k)\rangle=e^{\text{i}k}|\alpha(k)\rangle$.
$\mathcal{N}_{\alpha}$ is the normalization factor, which may be different from $L$ in the case $R<L$, where $R$ is the periodicity of $|\alpha\rangle$ defined by $\mathcal{T}^{R}|\alpha\rangle=\zeta_{R}|\alpha\rangle$, $MR=L, M\in\mathbb{Z}$.
$\zeta(R)=\pm{1}$ is the fermion sign accumulated in one periodic cycle $R$ translation. 
Reshape $i=l\cdot R+j$, then Eq. (\ref{eq:moment_state}) can be rewritten as
\begin{equation}
    \begin{aligned}
        &|\alpha(k)\rangle=\frac{1}{\sqrt{\mathcal{N}_{\alpha}}}\sum_{j=0}^{R-1}e^{-\text{i}kj}\mathcal{T}^{j}\left( \sum_{l=0}^{M-1}e^{-\text{i}kRl}\mathcal{T}^{l\cdot R} \right)|\alpha\rangle \\
        &=\frac{1}{\sqrt{\mathcal{N}_{\alpha}}}\sum_{j=0}^{R-1}e^{-\text{i}kj}\mathcal{T}^{j}\left( \sum_{l=0}^{M-1}e^{-\text{i}kRl}\zeta_{R}^{l} \right)|\alpha\rangle \\
        &=\frac{\Gamma}{\sqrt{\mathcal{N}_{\alpha}}}\sum_{j=0}^{R-1}e^{-\text{i}kj}\mathcal{T}^{j}|\alpha\rangle,
    \end{aligned}
    \label{eq:}
\end{equation}
where $\Gamma\equiv\sum_{l=0}^{M-1}e^{-\text{i}kRl}\zeta_{R}^{l}$. 
We would like to require $|\Gamma|^{2}R/\mathcal{N}_{\alpha}=1$ to keep the normalization of $\langle\alpha(k)|\alpha(k)\rangle=1$. 
If $|\Gamma|^{2}=0$, we say that $k$ is \emph{incompatible} with $|\alpha\rangle$ therefore $|\alpha\rangle$ will not appear in this very translational symmetric representation. 
Note that in bosonic case, $\Gamma=\sum_{l=0}^{M-1}e^{-\text{i}kRl}=M$ only if $kR=2n\pi, n\in\mathbb{Z}$. Otherwise $\sum_{l=0}^{M-1}e^{-\text{i}kRl}=0$. 
This can be viewed as an (unnormalized) discrete Fourier transformation of the array of signs $\{\zeta_{R}^{0}, \zeta_{R}^{1}, \dots, \zeta_{R}^{M-1}\}$. 
For Bosons, simply go as $\{1, \dots, 1\}\rightarrow\{M, 0, \dots, 0\}$ and $\langle\alpha(k)|\alpha(k)\rangle=\frac{1}{\mathcal{N}}RM^{2}=1$.
In this sense, a translational symmetric basis $|\alpha(k)\rangle$ is \emph{represented by a pair}, namely a representative configuration $|\alpha\rangle$, which is always choose as the smallest integer in one period, and its nonzero normalization factor $\mathcal{N}_{\alpha}=|\Gamma|^{2}R$.
It is also more convenient to record the \emph{position in the new representation, position in the translational period and the fermion sign of each original basis}.
Alternatively, you can store the whole configuration sequene, which will cost more memory but less computational time.

For two momentum states $|\alpha(k)\rangle, |\alpha^{\prime}(k)\rangle$, certainly $|\alpha^{\prime}\rangle\neq \mathcal{T}^{l}|\alpha\rangle, \forall~l$. 
When the Hamiltonian operates on the momentum state, we only need to consider its operation on the reference state because of $[H, \mathcal{T}]=0$.
Say
\begin{equation}
    \begin{aligned}
        &H|\alpha(k)\rangle=\frac{1}{\sqrt{\mathcal{N}_{\alpha}}}\sum_{j=0}^{N-1}e^{-\text{i}kj}\mathcal{T}^{j}H|\alpha\rangle \\
        &=\frac{1}{\sqrt{\mathcal{N}_{\alpha}}}\sum_{j=0}^{N-1}e^{-\text{i}kj}\mathcal{T}^{j}h|\gamma^{\prime}\rangle \\
        &=\frac{h}{\sqrt{\mathcal{N}_{\alpha}}}\sum_{j=0}^{N-1}e^{-\text{i}kj}\mathcal{T}^{(j-l)}\zeta_{l}|\gamma\rangle \\
        &=h\zeta_{l}e^{-\text{i}kl}\left(\frac{\mathcal{N}_{\gamma}}{\mathcal{N}_{\alpha}}\right)^{1/2}|\gamma(k)\rangle,
    \end{aligned}
    \label{eq:}
\end{equation}
where we have used the identity $\mathcal{T}^{l}|\gamma\rangle=\zeta_{l}|\gamma^{\prime}\rangle$ in the representation of $|\gamma(k)\rangle$. 
$h\equiv\langle\gamma^{\prime}|H|\alpha\rangle$ is the matrix element in the original Hilbert space. 
Arbitrary \emph{translational invariant observable} in translational symmetric basis acts similarly. 

\subsubsection{Two directions}
Suppose we have a Hubbard model on a $L_{x}\times L_{y}$ torus.
Translational operators can be defined as $\mathcal{T}_{x}c_{i\sigma}^{\dagger}\mathcal{T}_{x}^{-1}=c_{i+\hat{x}, \sigma}^{\dagger}$ and $\mathcal{T}_{y}c_{i\sigma}^{\dagger}\mathcal{T}_{y}^{-1}=c_{i+\hat{y}, \sigma}^{\dagger}$.
$[\mathcal{T}_{x},\mathcal{T}_{y}]=0$.
When they are operating on a reference state,
\begin{equation}
    \mathcal{T}_{y}^{m}\mathcal{T}_{x}^{l}|\alpha\rangle=\mathcal{T}_{x}c_{0}^{\dagger}c_{1}^{\dagger}\dots|0\rangle\equiv\zeta_{lm}|\alpha^{\prime}\rangle,
    \label{eq:transx_state}
\end{equation}
where $\zeta_{lm}=\zeta_{ml}=\pm 1$ is the fermion sign arised during these translations.
$\mathcal{T}_{x}^{L_{x}}|\alpha\rangle=|\alpha\rangle$.
Suppose the periodicities of the two directions are $R_{x, y}$ with $\mathcal{T}_{x, y}^{R_{x, y}}|\alpha\rangle=\zeta_{R_{x, y}}|\alpha\rangle$.
$L_{x}=M_{x}R_{x}, L_{y}=M_{y}R_{y}$.
Because of $[H, \mathcal{T}_{x, y}]=0$, under PBC we can construct basis with fixed translational quantum numbers $\mathbf{k}=(k_{x}=2\pi n_{x}/N_{x}, k_{y}=2\pi n_{y}/N_{y}), n_{x}=0, \dots, L_{x}-1; n_{y}=0, \dots, L_{y}-1$.
Reshape $i=l\cdot{R_{x}}+m, j=p\cdot{R_{y}}+q$.
Then
\begin{equation}
    \begin{aligned}
        &|\alpha(\mathbf{k})\rangle=\frac{1}{\sqrt{\mathcal{N}_{\alpha}}}\sum_{\mathbf{r}=i\hat{x}+j\hat{y}}e^{-\text{i}\mathbf{k}\cdot\mathbf{r}}\mathcal{T}_{y}^{j}\mathcal{T}_{x}^{i}|\alpha\rangle \\
        &=\frac{\Gamma_{y}\Gamma_{x}}{\sqrt{\mathcal{N}_{\alpha}}}\sum_{q=0}^{R_{y}-1}\sum_{m=0}^{R_{x}-1}e^{-\text{i}(k_{x}m+k_{y}q)}\mathcal{T}_{y}^{q}\mathcal{T}_{x}^{m}|\alpha\rangle \\
        &=\frac{\Gamma_{y}\Gamma_{x}}{\sqrt{\mathcal{N}_{\alpha}}}\sum_{q=0}^{R_{y}-1}e^{-\text{i}k_{y}q}\mathcal{T}_{y}^{q}\sum_{m=0}^{R_{x}-1}e^{-\text{i}k_{x}m}\mathcal{T}_{x}^{m}|\alpha\rangle \\
        &=\frac{\Gamma_{y}\Gamma_{x}}{\sqrt{\mathcal{N}_{\alpha}}}\sum_{q=0}^{R_{y}-1}e^{-\text{i}k_{y}q}\mathcal{T}_{y}^{q}|\tilde{\alpha}(k_{x})\rangle,
    \end{aligned}
    \label{eq:}
\end{equation}
where we have defined $\Gamma_{x}\equiv\sum_{l=0}^{M_{x}-1}e^{-\text{i}k_{x}R_{x}l}(\zeta_{R_{x}})^{l}, \Gamma_{y}\equiv\sum_{p=0}^{M_{y}-1}e^{-\text{i}k_{y}R_{y}p}(\zeta_{R_{y}})^{p}$.
To guarantee the normalization $\langle\alpha(\mathbf{k})|\alpha(\mathbf{k})\rangle=1$, we have $\mathcal{N}_{\alpha}=|\Gamma_{x}|^{2}|\Gamma_{y}|^{2}R_{x}R_{y}$.
In this sense, $|\alpha(\mathbf{k})\rangle$ can still be represented by a pair namely the reference state $|\alpha\rangle$ and its normalization factor $\mathcal{N}_{\alpha}$.
In another word, one translational invariant basis vector in the reduced Hilbert space actually is a \emph{sequence} of vectors in the original Hilbert space.
All of them can be obtained by translating the reference state in some way.
% $|\tilde{\alpha}(k_{x})\rangle$ is the unrenormalized momentum state along $\hat{x}$-direction.

When the Hamiltonian operates on the momentum state, we only need to consider its operation on the reference state because of $[H, \mathcal{T}_{x, y}]=0$.
Say
\begin{equation}
    \begin{aligned}
        &H|\alpha(\mathbf{k})\rangle=\frac{1}{\sqrt{\mathcal{N}_{\alpha}}}\sum_{\mathbf{r}=i\hat{x}+j\hat{y}}e^{-\text{i}\mathbf{k}\cdot\mathbf{r}}\mathcal{T}_{y}^{j}\mathcal{T}_{x}^{i}H|\alpha\rangle \\
        &=\frac{1}{\sqrt{\mathcal{N}_{\alpha}}}\sum_{\mathbf{r}=i\hat{x}+j\hat{y}}e^{-\text{i}\mathbf{k}\cdot\mathbf{r}}\mathcal{T}_{y}^{j}\mathcal{T}_{x}^{i}h|\gamma^{\prime}\rangle \\
        &=\frac{h}{\sqrt{\mathcal{N}_{\alpha}}}\sum_{\mathbf{r}=i\hat{x}+j\hat{y}}e^{-\text{i}\mathbf{k}\cdot\mathbf{r}}\mathcal{T}_{y}^{(j-m)}\mathcal{T}_{x}^{(i-l)}\zeta_{lm}|\gamma\rangle \\
        &=h\zeta_{lm}e^{-\text{i}(k_{x}l+k_{y}m)}\left( \frac{\mathcal{N}_{\gamma}}{\mathcal{N}_{\alpha}} \right)^{1/2}|\gamma(\mathbf{k})\rangle,
    \end{aligned}
    \label{eq:}
\end{equation}
where we have used the identity $\mathcal{T}_{y}^{m}\mathcal{T}_{x}^{l}|\gamma\rangle=\zeta_{lm}|\gamma^{\prime}\rangle$ in the representation of $|\gamma(\mathbf{k})\rangle$. 
$h=\langle\gamma^{\prime}|H|\alpha\rangle$ is the matrix element in the original Hilbert space. 
Arbitrary \emph{translational invariant observable} in translational symmetric basis acts similarly. 

For convenience to generate the Hamiltonian matrix in the reduced Hilbert space, we may introduce a \texttt{tag} for each basis vector in the original Hilbert space, which contains four elements:
\begin{itemize}
  \item 
    \texttt{idx}: index representing the position of the corresponding reference state in the reduced Hilbert space
  \item
    \texttt{sgn}: fermionic sign accumulated from the reference state to arrive this state
\end{itemize}



\bibliographystyle{apsrev4-2}
\bibliography{ref}
\end{document} 

