#include <iostream>
#include <fstream>
#include <random>
#include<iomanip>
#include <memory.h>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <cmath>
#include <stdlib.h>
#include <ctime>
#include <algorithm>
#include <initializer_list>
#include <vector>
#include <bitset>
#include <complex>
#include <map>

// Arpackpp
#include <arssym.h>
#include <arcomp.h>
#include <arscomp.h>

// boost
#include <boost/dynamic_bitset.hpp>

#include <Eigen/Core>
#include <Eigen/SparseCore>
#include <Spectra/GenEigsSolver.h>
#include <Spectra/MatOp/SparseGenMatProd.h>

int main();
