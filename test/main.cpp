#include "head.hpp"
#include "class_hubbard.hpp"

typedef double T;

int main() {
        const int numEleUp = 3;
        const int numEleDown = 3;
        double U = 1.0;
        HubbardTriangle<T> Hub (U, numEleUp, numEleDown);
        Hub.CSCHamMatrix ();
        int dim = Hub.Dim ();
        int nz = Hub.Nzeros ();
        Eigen::SparseMatrix<T> H (dim, dim);
        Hub.SetEigenMatrix (H);
        Spectra::SparseGenMatProd<T> OP (H);
        Spectra::GenEigsSolver<T, Spectra::SMALLEST_MAGN, Spectra::SparseGenMatProd<T>> eigs (&OP, 5, 10);
        eigs.init ();
        eigs.compute ();
        std::cout << eigs.eigenvalues () << std::endl;
        return 1;
    }
